------------------------------------------------------------------------------
ChainReplication Project README File
CSE 535 - Aysnchronous System 
------------------------------------------------------------------------------
Swijal Patil              - swapatil@cs.stonybrook.edu
Soujanya Shankaranarayana - soshankarana@cs.stonybrook.edu
------------------------------------------------------------------------------

[INSTRUCTIONS]

This section elaborates the steps to run a sample testcase. 

Step 1: To make sure all ports are available, run ./cleanup.sh 

Step 2:  For running test case we need to start the servers, clients and master. For that we have made launcher.sh which will start the servers, clients and master.

Step 3: For running Testcases as per different config files provided for each testing type, we need to run ./launcher.sh which will take the testcase file as input and process it.
	
Step 4: When the mentioned requests are completed as per test cases then the logs are generated in the logs/ directory with the name of the client and the servers as below. 
	For Clients: Client1.log, Client2.log
        For Servers: Bank 1 chain: Server1.1.log, Server1.2.log, Server1.3.log
		     Bank 2 chain: Server2.1.log, Server2.2.log, Server2.3.log
	For Masters: Master.log


[MAIN FILES]

Below are the main files of our source code. We have provided an overview of what it does. For details, source code can be refered.

Common Files:
----------------
ChainReplicationLogger.java	: Singleton logger class that logs to the log file of the invoking server/ client
ChainReplicationLogFormatter    : Used for formatting the log segment that is printed
Reply.java			: Data structure for sending messages to the client
Request.java			: Data structure that contains the requests data 
Config.java			: Contains all the parsing logic to read from config file and convert it into java usable variables.
launcher.sh			: Starts all the servers and the clients and instantiates them. Also parses the content from config file.
cleanup.sh			: Used for killing all the active connections on the ports that will be used for the project. 
Configuration files		: configuration files for various test cases as per requirement are as below:
				  head_crash.xml tail_crash.xml intermediate_crash.xml basic_extend.xml extend_tail_crash.xml 
				  extend_node_crash.xml intermediate_succ_crash.xml pred_intermediate_crash.xml ack_empty_sentlist.xml and
				  message_loss.xml

Client File: (client package)
----------------
Client.java			: Main java program to request to the server and receive acknowledgement
TcpClientFailureDetectThread	: Thread that listens to failure realted messages from server	
UdpClientReceiveThread		: Thread that receives reply from tail of server chain
UdpClientSendThread		: Thread that sends requests to head of server chain


Server Files:
----------------
Server.java			: Main java program to invoke the servers and perform the requests received from client and reply back 					  to client
TcpAckReceiveThread		: Thread for receiving acknowledgements from successor
TcpAckSendThread		: Thread for sending acknowledgement to predecessor
TcpFailureDetectThread		: Thread for listening failure/extend messages from master
TcpReqReceiveThread		: Thread for receiving requests from predecessor
TcpReqSendThread		: Thread for sending requests to successor
UdpKeepAliveSendThread		: Thread for sending keep alive messages to master
UdpRepSendThread		: Thread for sending reply messages to clients
UdpReqReceiveThread		: Thread for receiving requests from clients

Master File:
----------------
Master.java			: Master is responsible to handle the failure and extending chain functionality
UdpKeepAliveReceiveThread	: Thread for listening for keep alives from server

util:
----------------
Account.java			: Serializable Data structure for storing the account details and passing account details 
				  during chain extension.
FailureUpdateClientRequest	: Serializable class used for communicating failure to clients
FailureUpdateServerRequest	: Serializable class used for communicating failure to servers
Request				: Serializable Bank operation requests
Reply				: Serializable Bank operation replies

config
----------------
ClientConfig			: Configuration for Client read from XML
MasterConfig			: Configuration for Master read from XML
ServerConfig			: Configuration for Server read from XML


[CONTRIBUTIONS]

Soujanya Shankaranarayana       : Entire functionaltiy of project in Java
Swijal				: Basic TCP/UDP connection


[OTHER COMMENTS]

- We have written a script launcher.sh for starting all the servers, master and clients which will help to start all the servers and clients.
 We have also prepared script cleanup.sh which will basically kill all the process running on servers and clients which inturn making ports available.  

- Log files will be generated under logs folder which will have naming convention as mentioned above. Logs will contain the overall flow happening in server or client as per the file. There will be seperate log files for each server and client. 



[FUNCTIONALITIES IMPLEMENTED]

1. Specification and creation of multiple chains for different banks 

2. Specification of multiple clients and servers associated to each bank 

3. Generation of itemized requests from the client 

4. Retransmission of requests from the client in an event of no response from the server

5. After some fixed number of retires request is failed, client is acknowledged and it cannot be processed further.

6. Handling GetBalance functionality which will return balance to the client for a specific account number. In case of account not present it will create one and return balance 0.

7. Handling deposit functionality at the server. If the account is not present, a new one is created using the account number specified in the request and the specificed amount is deposited in the account.

8. Handling withdrawal functionality at the server. If the account is not present, account will be created and client will be replied as insufficient balance as new account will have balance 0. Also Insufficient balance functionality is checked while withdrawing and ack accordingly.

9. Duplicate requests are handled along with the inconsistent with History requests and acknowledged accordingly to the client.

10. Handling Head crash.

11. Handling Tail crash.

12. Handling Intermediate server crash.

13. Handling Predecessor server crash during Intermediate server crash.

14. Handling Successor server crash during Intermediate server crash.

15. Extending the chain.

16. Handling tail failure during extending chain functionaltiy.

17. Handling extend node failure during extending chain functionaltiy.

18. Use of ack to remove old updates from sent sets.

19. Handling message loss from client to server.
