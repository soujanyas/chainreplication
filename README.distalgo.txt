------------------------------------------------------------------------------
ChainReplication Project README File
CSE 535 - Aysnchronous System 
------------------------------------------------------------------------------
Swijal Patil              - swapatil@cs.stonybrook.edu
Soujanya Shankaranarayana - soshankarana@cs.stonybrook.edu
------------------------------------------------------------------------------



[INSTRUCTIONS]

This section elaborates the steps to run a sample testcase. 

Step 1: To initiate all the servers and clients we have a common file launcher.da which will instantiate all the servers and clients. 

Step 2: Once the servers and clients are started, client will read input from config file (test case) and then will send it to server for processing. Server inturn will process and ack back.

Step 3: For running Test Case mentioned in testing.txt, we need to run launcher.da which will take the testcase file as input and process it.
	
Step 4: When the mentioned requests are completed as per test cases then the logs are generated in the logs/client and logs/server directory with the name of the client and the servers.



[MAIN FILES]

Below are the main files of our source code. We have provided an overview of what it does. For details, source code can be refered.

Common Files:
----------------
launcher.da			: Parsing the input from config file and interpreting it accordingly. Also it is used for starting servers and 					  clients.
conf_parser.py			: Parsing logic to read from XML and save it in local variables.
data_types.py			: Defines the structure and attributes of variables used to store data (Request and Reply).
basic_config.xml		: Configuration file

Client File:
----------------
client.da			: Main "da" program to request to the server and receive acknowledgement

Server Files:
----------------
server.da			: Main "da" program to invoke the servers and perform the requests received from client and reply back 					  to client



[BUGS AND LIMITATIONS]:

Request numbers are generated as client.bank.seq_no, here we haven't implemented random number generator so the requests will be always generated based on client id, bank id and sequence number.




[CONTRIBUTIONS]:

Soujanya Shankaranarayana       : Client logic along with parsing and network connections
Swijal Patil			: Server logic along with logging and network connections




[OTHER COMMENTS]:

- Project contains master.da which plays no part in the current functionality of the project. File is with repect to future prepective where we will require master in phase 3. 




[FUNCTIONALITIES IMPLEMENTED]:

1. Specification and creation of multiple chains for different banks 

2. Specification of multiple clients and servers associated to each bank 

3. Generation of itemized requests from the client 

4. Retransmission of requests from the client in an event of no response from the server

5. After some fixed number of retires request is failed, client is acknowledged and it cannot be processed further.

6. Handling GetBalance functionality which will return balance to the client for a specific account number. In case of account not present it will create one and return balance 0.

7. Handling deposit functionality at the server. If the account is not present, a new one is created using the account number specified in the request and the specificed amount is deposited in the account.

8. Handling withdrawal functionality at the server. If the account is not present, account will be created and client will be replied as insufficient balance as new account will have balance 0. Also Insufficient balance functionality is checked while withdrawing and ack accordingly.

9. Duplicate requests are handled along with the inconsistent with History requests and acknowledged accordingly to the client.
