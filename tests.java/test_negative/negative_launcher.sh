#!/bin/bash

set -o verbose


PROJECT_HOME=/home/soujanya/academics/async/chain-replication/java-chain-repl/

SRC_HOME=$PROJECT_HOME/src
CONFIG_FILE=$PROJECT_HOME/config/negative_config.xml

cd $SRC_HOME
javac Server.java
javac Client.java

export CLASSPATH=$SRC_HOME
# FORMAT: java -ea Server <server_num> <bank_num> <config_file>
java -ea Server 3 1 $CONFIG_FILE &
java -ea Server 2 1 $CONFIG_FILE &
java -ea Server 1 1 $CONFIG_FILE &

java -ea Client 1 $CONFIG_FILE &
