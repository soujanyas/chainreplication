#!/bin/bash

set -o verbose

PROJECT_HOME=/home/soujanya/academics/async/chain-replication/java-chain-repl/
SRC_HOME=$PROJECT_HOME/src
CONFIG_FILE=$PROJECT_HOME/config/multi_server_client_config.xml

cd $SRC_HOME
javac Server.java
javac Client.java

export CLASSPATH=$SRC_HOME
java -ea Server 3 1 $CONFIG_FILE &
java -ea Server 2 1 $CONFIG_FILE &
java -ea Server 1 1 $CONFIG_FILE &

java -ea Server 3 2 $CONFIG_FILE &
java -ea Server 2 2 $CONFIG_FILE &
java -ea Server 1 2 $CONFIG_FILE &

java -ea Client 1 $CONFIG_FILE &
java -ea Client 2 $CONFIG_FILE &
java -ea Client 3 $CONFIG_FILE &
