package master;

import config.ClientConfig;
import config.MasterConfig;
import config.ServerConfig;
import logger.ChainReplicationLogger;
import server.ServerInfo;
import util.Constants;
import util.FailureUpdateClientRequest;
import util.FailureUpdateServerRequest;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Master Component of chain replication.
 */
public class Master {
    MasterConfig masterConfig;
    Map<Integer, Map<Integer, Long>> timeTrackerMap;
    Map<Integer, List<ServerInfo>> serverMap;
    Map<Integer, Map<Integer, ServerConfig>> serverConfigMap;
    Map<Integer, ClientConfig> clientConfigMap;

    boolean crashPred;
    boolean crashSucc;
    boolean crashTailExtend;
    boolean crashNodeExtend;

    private static Logger logger;

    // A map keyed on client id and having socket connection to client as a value
    Map<Integer, ObjectOutputStream> clientSocketMap;
    // Map of port number vs object outstream
    Map<Integer, ObjectOutputStream> serverSocketMap;

    public boolean extensionInProgress = false;
    public ServerInfo serverExtended;

    public Master(String configFile) {
        masterConfig = new MasterConfig(configFile);
        // Initialize the initial topology for server
        ServerConfig serverConfig = new ServerConfig(configFile);
        serverMap = serverConfig.serverInfoMap;
        serverConfigMap = serverConfig.serverConfigMap;
        // Initialize maps
        clientSocketMap = new HashMap<Integer, ObjectOutputStream>();
        serverSocketMap = new HashMap<Integer, ObjectOutputStream>();
        timeTrackerMap = new HashMap<Integer, Map<Integer, Long>>();

        crashPred = masterConfig.crashPred;
        crashSucc = masterConfig.crashSucc;
        crashNodeExtend = masterConfig.crashNodeExtend;
        crashTailExtend = masterConfig.crashTailExtend;
        logger = ChainReplicationLogger.myLogger;


        // For each bank
        for (Map.Entry<Integer, List<ServerInfo>> entry : serverMap.entrySet()) {
            List<ServerInfo> list = entry.getValue();
            Map<Integer, Long> timeMap = new HashMap<Integer, Long>();

            // Initialize last ping time for all servers.
            for (ServerInfo info : list) {
                timeMap.put(info.serverId, 0l);
            }
            // Add timemap of a bank
            timeTrackerMap.put(entry.getKey(), timeMap);
        }
        // Client related information
        clientConfigMap = new ClientConfig(configFile).clientConfigMap;

        System.out.println("timeTrackerMap: " + timeTrackerMap);
    }

    private static void help() {
        System.out.println("Usage: Master <config_file>");
    }

    /**
     * Method used to update predecessor/successor on failure detection.
     */
    private void updateServers(String type, ServerInfo toUpdateServerInfo, Integer newServerId,
                               Integer bankId, boolean crashServer) {

        try {
            ObjectOutputStream objectOutputStream;
            Integer failure_tcp_in = toUpdateServerInfo.failure_tcp_in;

            // Ensure we use the same socket connection for all communication for a given TCP port.
            if (serverSocketMap.get(failure_tcp_in) == null) {
                Socket tcpSocket = new Socket(toUpdateServerInfo.failure_tcp_ip, failure_tcp_in);
                objectOutputStream = new ObjectOutputStream(tcpSocket.getOutputStream());
                serverSocketMap.put(failure_tcp_in, objectOutputStream);
            } else {
                objectOutputStream = serverSocketMap.get(failure_tcp_in);
            }


            Integer port = -1;
            Integer ackPort = -1;

            // Predecessor update.predecessor ports are valid only if intermediate node crashed.
            // For head and tail (newServerId = -1) it is required only to say newServerId = -1
            if (newServerId != -1) {
                ServerConfig newServerConfig = serverConfigMap.get(bankId).get(newServerId);
                // Predecessor has to update successor TCP and ack port and successor has to update predecessor TCP and
                // ack port.

                // |   1 |          2   |           3   |
                //       |1500          | 1600          |
                // 1 new port-> 1600
                if (type.equals(Constants.updatePred) || type.equals(Constants.updatePredExtend)) {
                    // Continue to receive requests on same port
                    ackPort = newServerConfig.ackRecvPort;
                } else {
                    port = newServerConfig.reqRecvPort;
                    // Continue to receive ack on same port
                }
            }
            // Send the failure notification request.
            FailureUpdateServerRequest req = new FailureUpdateServerRequest(type, newServerId, port, ackPort,
                    "localhost", crashServer);
            objectOutputStream.writeObject(req);
            objectOutputStream.flush();
            ChainReplicationLogger.myLogger.log(Level.INFO, "Updating on Failure [" + req + "] sent to " + toUpdateServerInfo + " for processing");
            System.out.println("Updating on Failure [" + req + "] sent to " + toUpdateServerInfo + " for processing");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
     * Method used to update clients of its new head and tail
     */
    private void updateClients(String updateType, ServerInfo serverInfo, Integer bankId) {

        System.out.println("Server info:" + serverInfo + " updateType:" + updateType);

        for (Map.Entry<Integer, ClientConfig> client : clientConfigMap.entrySet()) {
            try {
                Integer clientId = client.getKey();
                Integer tcpPort = client.getValue().failue_tcp_in;
                ObjectOutputStream objectOutputStream = null;

                if (clientSocketMap.get(clientId) == null) {
                    Socket tcpSocket = null;

                    boolean notConnected = true;
                    while (notConnected) {
                        try {
                            tcpSocket = new Socket("localhost", tcpPort);
                        } catch (ConnectException e) {
                            System.out.println("Waiting for client failure handler listener..");
                            Thread.yield();
                            Thread.sleep(1000);
                            continue;
                        }
                        notConnected = false;
                    }
                    objectOutputStream = new ObjectOutputStream(tcpSocket.getOutputStream());
                    clientSocketMap.put(clientId, objectOutputStream);
                } else {
                    // Socket map entry is already created.
                    objectOutputStream = clientSocketMap.get(clientId);
                }

                System.out.println("Client socket map:" + clientSocketMap);

                // New head/tail port
                Integer newUdpPort = serverConfigMap.get(bankId).get(serverInfo.serverId).udpIn;

                // Construct a client request and send it across to all the clients
                FailureUpdateClientRequest request = new FailureUpdateClientRequest(updateType, bankId, newUdpPort);
                objectOutputStream.writeObject(request);
                objectOutputStream.flush();

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param bankId   Bank Id for the server for which failure is being handled.
     * @param serverId Server Id for the server for which failure is being handled.
     */
    private void handleFailure(Integer bankId, Integer serverId) {
        System.out.println("Declaring server:" + serverId + " for bank:" + bankId + " dead.. :( ");
        ArrayList<ServerInfo> serverChain = (ArrayList<ServerInfo>) serverMap.get(bankId);
        int i = 0;
        ServerInfo toRemoveServerInfo = null;

        if (extensionInProgress && serverId == serverExtended.serverId) {
            // If new node crashed when extension is in progress
            logger.log(Level.INFO, "Extending node crashed when extension was in progress");
            updateServers(Constants.updateSucc, getTail(bankId), -1, bankId, false);
            updateClients(Constants.updateTail, getTail(bankId), bankId);
            extensionInProgress = false;
            return;
        }

        for (ServerInfo serverInfo : serverChain) {
            System.out.println("Checking Server chain for failure:" + serverInfo.serverId);
            if (serverInfo.serverId == serverId) {
                // HEAD crashed
                if (i == 0) {
                    System.out.println("HEAD crashed serverId:" + serverId);
                    logger.log(Level.INFO, "HEAD crashed serverId:" + serverId);
                    // TODO: When there is only one node in the chain and that crashes
                    updateServers(Constants.updatePred, serverChain.get(i + 1), -1, bankId, false);
                    updateClients(Constants.updateHead, serverChain.get(i + 1), bankId);
                }
                // TAIL crashed
                else if (i == serverChain.size() - 1) {
                    System.out.println("TAIL crashed serverId:" + serverId);
                    logger.log(Level.INFO, "TAIL crashed serverId:" + serverId);
                    if (extensionInProgress) {
                        logger.log(Level.INFO, "Tail crashed when extension in progress");
                        // TODO: Corner cases when single node is left..
                        updateServers(Constants.updateSuccExtend, serverChain.get(i - 1), serverExtended.serverId,
                                bankId, false);
                        updateServers(Constants.updatePredExtend, serverExtended, serverChain.get(i - 1).serverId,
                                bankId, false);
                    } else {
                        updateServers(Constants.updateSucc, serverChain.get(i - 1), -1, bankId, false);
                        updateClients(Constants.updateTail, serverChain.get(i - 1), bankId);
                    }
                }
                //Intermediate server crashed
                else {
                    System.out.println("INTERMEDIATE server crashed serverId:" + serverId);
                    logger.log(Level.INFO, "INTERMEDIATE server crashed serverId:" + serverId);
                    updateServers(Constants.updatePred, serverChain.get(i + 1), serverChain.get(i - 1).serverId,
                            bankId, crashSucc);
                    updateServers(Constants.updateSucc, serverChain.get(i - 1), serverChain.get(i + 1).serverId,
                            bankId, crashPred);
                    crashSucc = crashPred = false; // We crash S- an S+ only once.
                }
                toRemoveServerInfo = serverInfo;
                break;
            }
            i++;
        }

        serverChain.remove(toRemoveServerInfo);
    }

    private ServerInfo getTail(Integer bankId) {
        List<ServerInfo> serverInfos = serverMap.get(bankId);
        return serverInfos.get(serverInfos.size() - 1);

    }

    public void extendChainDone(ServerInfo newServerInfo) {
        System.out.println("Extend chain Done received..");
        Integer bankId = newServerInfo.bankId;
        // Update master and clients.

        updateServers(Constants.extendDone, getTail(newServerInfo.bankId), newServerInfo.serverId, bankId, false);
        updateClients(Constants.updateTail, newServerInfo, bankId);
        serverMap.get(bankId).add(newServerInfo); // Add to chain
    }

    /**
     * Method that encapsulates logic for adding a new node to the server chain
     *
     * @param newServerInfo new server to be added to the chain
     */
    public void extendChainServer(ServerInfo newServerInfo) {
        Integer bankId = newServerInfo.bankId;

        // Update servers only
        updateServers(Constants.updateSuccExtend, getTail(bankId), newServerInfo.serverId, bankId, crashTailExtend);
        updateServers(Constants.updatePredExtend, newServerInfo, getTail(bankId).serverId, bankId, crashNodeExtend);
    }


    private void validateServerAlive() {
        for (Map.Entry<Integer, Map<Integer, Long>> bankEntry : timeTrackerMap.entrySet()) {
            Integer bankId = bankEntry.getKey();
            Integer failedServer = -1;
            Map<Integer, Long> bankMap = timeTrackerMap.get(bankId);
            for (Map.Entry<Integer, Long> serverEntry : bankMap.entrySet()) {
                Long lastPingTime = serverEntry.getValue();
                Integer serverId = serverEntry.getKey();
                // Check if master received keep alive message from this bank in past so many milli seconds.
                if (System.currentTimeMillis() - lastPingTime > masterConfig.wakeUpIntervalMs) {
                    handleFailure(bankId, serverId);
                    failedServer = serverId;
                }
            }
            // Remove failed entry from server map.
            if (failedServer != -1) {
                bankMap.remove(failedServer);
            }
        }
    }

    public void runMaster() {
        Thread keepAliveReceiveThread = new Thread(new UdpKeepAliveReceiveThread(this));
        keepAliveReceiveThread.start();

        while (true) {
            try {
                Thread.sleep(masterConfig.wakeUpIntervalMs);
                validateServerAlive();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public static void main(String[] args) {
        if (args.length < 1) {
            help();
        }
        String config_fle = args[0];

        String file_name = "Master.log";
        ChainReplicationLogger chainReplicationLogger = ChainReplicationLogger.getInstance(file_name);
        chainReplicationLogger.myLogger.log(Level.INFO, "Master Initialized");

        Master master = new Master(config_fle);
        master.runMaster();
    }
}
