package master;

import logger.ChainReplicationLogger;
import server.ServerInfo;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Map;
import java.util.logging.Level;

/**
 * Created by soujanya on 11/1/14.
 */
public class UdpKeepAliveReceiveThread implements Runnable {

    private Master master;

    public UdpKeepAliveReceiveThread(Master master) {
        this.master = master;
    }

    @Override
    public void run() {
        DatagramSocket receiveServerSocket;
        try {
            receiveServerSocket = new DatagramSocket(master.masterConfig.udpIn);
            while (true) {

                // initialize bytes array for storing sending and receiving data
                byte[] receiveData = new byte[1024];
                DatagramPacket receiveClientPacket = new DatagramPacket(receiveData, receiveData.length);
                receiveServerSocket.receive(receiveClientPacket);

                byte[] receivedDataAsObject = receiveClientPacket.getData();
                ByteArrayInputStream in = new ByteArrayInputStream(receivedDataAsObject);
                ObjectInputStream is = new ObjectInputStream(in);
                ServerInfo serverInfo = (ServerInfo) is.readObject();
                // ChainReplicationLogger.myLogger.log(Level.INFO, "Keepalive: ServerInfo [" + serverInfo + "] received for processing");
                // System.out.println("Keepalive: ServerInfo [" + serverInfo + "] received for processing");

                if (master.timeTrackerMap.containsKey(serverInfo.bankId)) {
                    Map<Integer, Long> bankTimeTracker = master.timeTrackerMap.get(serverInfo.bankId);

                    // If a keep alive was received for a server not in tracker map, extend it..
                    if (!bankTimeTracker.containsKey(serverInfo.serverId) && !master.extensionInProgress) {
                        System.out.println("Extending chain with new server : " + serverInfo);
                        master.extensionInProgress = true;
                        master.serverExtended = serverInfo;
                        master.extendChainServer(serverInfo);
                    }
                    bankTimeTracker.put(serverInfo.serverId, System.currentTimeMillis());
                    // A special UDP keep alive indicating chain extension is done.
                    if (serverInfo.specialFlag != null) {
                        master.extendChainDone(serverInfo);
                    }
                }
                Thread.yield();
            }

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
