package util;

import java.io.Serializable;

public class Reply implements Serializable {

    public String request_id;
    public String status;
    public float balance;
    public Integer client_id;


    public Reply(String status, float balance, Integer client_id, String request_id) {
        this.status = status;
        this.balance = balance;
        this.client_id = client_id;
        this.request_id = request_id;
    }

    public Reply() {
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Reply{" +
                "request_id='" + request_id + '\'' +
                ", status='" + status + '\'' +
                ", balance=" + balance +
                ", client_id=" + client_id +
                '}';
    }

}