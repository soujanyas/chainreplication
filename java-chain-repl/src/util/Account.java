package util;

import util.Reply;
import util.Request;

import java.io.Serializable;
import java.util.Map;

public class Account implements Serializable {
    public float balance;
    public Integer accountNumber;
    public Map<Request, Reply> history;

    public Account(Integer accountNum, float balance, Map<Request, Reply> history) {
        this.accountNumber = accountNum;
        this.balance = balance;
        this.history = history;
    }

    public float getBalance() {
        return balance;
    }
    public void setBalance(float balance) {
        this.balance = balance;
    }
    public Map<Request, Reply> getHistory() {
        return history;
    }
    public void setHistory(Map<Request, Reply> history) {
        this.history = history;
    }




}