package util;

import java.io.Serializable;

/**
 * Object class used to communicate a server failure.
 */
public class FailureUpdateServerRequest implements Serializable {
    public String updateType;
    public Integer newServer;
    public Integer newPort;
    public Integer newAckPort;
    public String newIp;
    public boolean crashServer;

    @Override
    public String toString() {
        return "FailureUpdateServerRequest{" +
                "updateType='" + updateType + '\'' +
                ", newServer=" + newServer +
                ", newPort=" + newPort +
                ", newAckPort=" + newAckPort +
                ", newIp='" + newIp + '\'' +
                ", crashServer=" + crashServer +
                '}';
    }

    public FailureUpdateServerRequest(String updateType, Integer newServer, Integer newPort, Integer newAckPort,
                                      String newIp, boolean crashServer) {
        this.updateType = updateType;
        this.newServer = newServer;
        this.newPort = newPort;
        this.newAckPort = newAckPort;
        this.newIp = newIp;
        this.crashServer = crashServer;
    }

}
