package util;

/**
 * Created by soujanya on 11/2/14.
 */
public class Constants {

    public static final String updateSucc = "update_succ";
    public static final String updatePred = "update_pred";
    public static final String updateSuccExtend = "update_succ_extend";
    public static final String updatePredExtend = "update_pred_extend";
    public static final String extendDone = "extend_done";
    public static final String updateHead = "update_head";
    public static final String updateTail = "update_tail";
    public static final String unboundShutdown = "unbounded";
    public static final String receiveShutdown = "receive";
    public static final String sendShutdown = "send";
    public static final String operationSyncAccounts = "sync_accounts";
    public static final String operationSyncAccountsDone = "sync_accounts_done";

}