package util;

import java.io.Serializable;

/**
 * Class used for communicating Server Failures to clients.
 */
public class FailureUpdateClientRequest implements Serializable{
    public String updateType;
    public Integer bankNum;
    public Integer newPort;

    public FailureUpdateClientRequest(String updateType, Integer bankNum, Integer newPort){
        this.updateType = updateType;
        this.bankNum = bankNum;
        this.newPort = newPort;
    }

    @Override
    public String toString() {
        return "FailureUpdateClientRequest{" +
                "updateType='" + updateType + '\'' +
                ", bankNum=" + bankNum +
                ", newPort=" + newPort +
                '}';
    }
}
