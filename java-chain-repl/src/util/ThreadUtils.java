package util;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by soujanya on 11/6/14.
 */
public class ThreadUtils {

    public static void closeServerSocket(ServerSocket socket){
        if (socket != null && !socket.isClosed()) {
            try {
                socket.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void closeClientSocket(Socket socket){
        if (socket != null && !socket.isClosed()) {
            try {
                socket.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
