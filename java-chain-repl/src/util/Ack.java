package util;

import java.io.Serializable;

/**
 * Class representing acknowledgment of messages
 */
public class Ack implements Serializable {
    public String req_id;
    public int account_number;

    public Ack(Integer account_number, String req_id){
        this.account_number = account_number;
        this.req_id = req_id;
    }

    @Override
    public String toString() {
        return "Ack{" +
                "req_id='" + req_id + '\'' +
                ", account_number=" + account_number +
                '}';
    }
}
