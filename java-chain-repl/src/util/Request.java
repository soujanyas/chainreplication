package util;

import java.io.Serializable;

public class Request implements Serializable {
    public String req_id;
    public String operation;
    public int account_number;
    public int amount;
    public int bank_num;
    public int client_id;
    // Request for account_update requests. Null otherwise. TODO: Test Request super class.
    public Account account;

    public Request() {
    }

    public Request(String req_id, String operation) {
        this.req_id = req_id;
        this.operation = operation;
    }

    public Request(String req_id, String operation, int account_number, Account account) {
        this.req_id = req_id;
        this.operation = operation;
        this.account_number = account_number;
        this.account = account;
    }

    public Request(String req_id, String operation, int account_number, int amount) {
        this.req_id = req_id;
        this.operation = operation;
        this.account_number = account_number;
        this.amount = amount;
    }

    public String getReq_id() {
        return req_id;
    }

    public void setReq_id(String req_id) {
        this.req_id = req_id;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public int getAccount_number() {
        return account_number;
    }

    public void setAccount_number(int account_number) {
        this.account_number = account_number;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String toString() {
        return "REQUEST : req_id: " + req_id + " operation: " + operation + " * account_number: " + account_number + " * amount: " + amount;
    }

    public boolean equals(Object o) {
        Request other = (Request) o;
        return other.req_id.equals(this.req_id)
                && other.operation.equals(this.operation)
                && other.account_number == this.account_number
                && other.amount == this.amount;

    }

}