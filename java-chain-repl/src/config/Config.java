package config;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import server.ServerInfo;
import util.Request;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Config {

    public static DocumentBuilder getBuilder() throws ParserConfigurationException {
        // Create Document builder.
        DocumentBuilderFactory builderFactory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        return builder;
    }

    public static void main(String[] args) {
        ClientConfig clientConfig = null;
        ServerConfig serverConfig = null;
        //clientConfig = new ClientConfig("/home/soujanya/academics/async/chain-replication/java-chain-repl/config/basic-config.xml", 1);
        serverConfig = new ServerConfig("/home/soujanya/academics/async/chain-replication/java-chain-repl/config/basic-config.xml", 1, 1);
        System.out.println(serverConfig);
        // serverConfig = new ServerConfig("/home/soujanya/academics/async/chain-replication/java-chain-repl/config/basic-config.xml");

        //System.out.println(clientConfig);
        //System.out.println(serverConfig.serverInfoMap);
        //System.out.println(serverServerConfigMap);

        //ClientConfig clientConfig1 = new ClientConfig("/home/soujanya/academics/async/chain-replication/java-chain-repl/config/basic-config.xml");
        //System.out.println(clientConfig1.clientConfigMap);
    }


}
