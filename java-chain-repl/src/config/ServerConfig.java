package config;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import server.ServerInfo;
import util.Constants;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* */
public class ServerConfig {

    private Integer predecessor;
    private Integer successor;

    public int reqSendPort_succ;
    public String succIp;
    public int reqRecvPort;
    public String predIp;

    public int ackSendPort_pred;
    public int ackRecvPort;

    public Map<Integer, Integer> clientPortMap;

    public int udpIn;
    public int udpOut;
    public int tcpFailPort;
    public String tcpFailIp = "localhost";

    public String shutdownType = Constants.unboundShutdown;
    public Integer shutdownCount = Integer.MAX_VALUE;

    public Integer startupDelaySec = 0;

    public Map<Integer, List<ServerInfo>> serverInfoMap;
    public Map<Integer, Map<Integer, ServerConfig>> serverConfigMap;

    public Integer messageLoss = 0;

    /**
     * Constructor for getting a list of servers in the topology. Used by master.
     *
     * @param configFile
     */
    public ServerConfig(String configFile) {
        Document document;
        try {
            DocumentBuilder builder = Config.getBuilder();
            document = builder.parse(new FileInputStream(configFile));
            XPath xPath = XPathFactory.newInstance().newXPath();
            String servers_expr = "/xml/servers";

            // Construct a map of server info required by master.
            String server_expr = "/xml/servers/server";
            NodeList servers = (NodeList) xPath.compile(server_expr).evaluate(document, XPathConstants.NODESET);
            serverInfoMap = new HashMap<Integer, List<ServerInfo>>();
            serverConfigMap = new HashMap<Integer, Map<Integer, ServerConfig>>();

            for (int i = 0; i < servers.getLength(); i++) {
                Node serverNode = servers.item(i);
                int serverId = Integer.parseInt(serverNode.getAttributes().getNamedItem("id").getNodeValue());
                int bankId = Integer.parseInt(serverNode.getParentNode().getAttributes().getNamedItem("bank").getNodeValue());
                // Get failure udp port for that server from that bank.
                String fail_tcp_expr = "/xml/servers[@bank=" + bankId + "]/server[@id=" + serverId + "]/failure_tcp_in";
                String extend_expr = "/xml/servers[@bank=" + bankId + "]/server[@id=" + serverId + "]/extended_node";
                int tcpFailureIn = Integer.parseInt(xPath.compile(fail_tcp_expr).evaluate(serverNode.getOwnerDocument()));
                boolean extendNode = Boolean.parseBoolean(xPath.compile(extend_expr).evaluate(serverNode.getOwnerDocument()));
                ServerInfo serverInfo = new ServerInfo(serverId, bankId, tcpFailureIn, "localhost");
                // Construct serverConfig map
                ServerConfig serverConfig = new ServerConfig(configFile, serverId, bankId);
                List<ServerInfo> serverInfoList;

                if (serverInfoMap.get(bankId) == null) {
                    serverInfoList = new ArrayList<ServerInfo>();
                    serverInfoMap.put(bankId, serverInfoList);
                    // Update the server config
                    Map<Integer, ServerConfig> serverConfigMapPerBank = new HashMap<Integer, ServerConfig>();
                    serverConfigMapPerBank.put(serverId, serverConfig);
                    serverConfigMap.put(bankId, serverConfigMapPerBank);
                } else {
                    serverInfoList = serverInfoMap.get(bankId);
                    serverConfigMap.get(bankId).put(serverId, serverConfig);
                }
                if (!extendNode) {
                    serverInfoList.add(serverInfo);
                }
            }

        } catch (SAXException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

    }

    @Override
    public String toString() {
        return "ServerConfig{" +
                "predecessor=" + predecessor +
                ", successor=" + successor +
                ", reqSendPort_succ=" + reqSendPort_succ +
                ", succIp='" + succIp + '\'' +
                ", reqRecvPort=" + reqRecvPort +
                ", predIp='" + predIp + '\'' +
                ", ackSendPort_pred=" + ackSendPort_pred +
                ", ackRecvPort=" + ackRecvPort +
                ", clientPortMap=" + clientPortMap +
                ", udpIn=" + udpIn +
                ", udpOut=" + udpOut +
                ", tcpFailPort=" + tcpFailPort +
                ", shutdownType='" + shutdownType + '\'' +
                ", shutdownCount=" + shutdownCount +
                ", serverInfoMap=" + serverInfoMap +
                ", serverConfigMap=" + serverConfigMap +
                '}';
    }

    public ServerConfig(String config_file, int server_num, int bank_num) {
        Document document;
        try {
            DocumentBuilder builder = Config.getBuilder();
            document = builder.parse(new FileInputStream(config_file));
            XPath xPath = XPathFactory.newInstance().newXPath();
            String server_expr = "/xml/servers[@bank=" + bank_num + "]/server[@id=" + server_num + "]";
            Node serverNode = (Node) xPath.compile(server_expr).evaluate(document, XPathConstants.NODE);

            // PARSE SERVER CONFIG
            NodeList nodes = serverNode.getChildNodes();
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                String name = node.getNodeName();
                if (name.equals("pred_tcp_port"))
                    this.reqRecvPort = Integer.parseInt(node.getTextContent());
                else if (name.equals("pred_ip"))
                    this.predIp = node.getTextContent();
                else if (name.equals("succ_tcp_port"))
                    this.reqSendPort_succ = Integer.parseInt(node.getTextContent());
                else if (name.equals("succ_ip"))
                    this.succIp = node.getTextContent();
                else if (name.equals("udp_in"))
                    this.udpIn = Integer.parseInt(node.getTextContent());
                else if (name.equals("failure_tcp_in"))
                    this.tcpFailPort = Integer.parseInt(node.getTextContent());
                else if (name.equals("pred_ack_port"))
                    this.ackSendPort_pred = Integer.parseInt(node.getTextContent());
                else if (name.equals("succ_ack_port"))
                    this.ackRecvPort = Integer.parseInt(node.getTextContent());
                else if (name.equals("shutdown_type"))
                    this.shutdownType = node.getTextContent();
                else if (name.equals("shutdown_count"))
                    this.shutdownCount = Integer.parseInt(node.getTextContent());
                else if (name.equals("startup_delay"))
                    this.startupDelaySec = Integer.parseInt(node.getTextContent());
                else if (name.equals("message_loss_precent"))
                    this.messageLoss = Integer.parseInt(node.getTextContent());
            }

            this.predecessor = Integer.parseInt(serverNode.getAttributes().getNamedItem("pred").getNodeValue());
            this.successor = Integer.parseInt(serverNode.getAttributes().getNamedItem("succ").getNodeValue());

            // PARSE CLIENT PORT INFORMATION FOR SENDING RESPONSES.
            String client_expr = "/xml/clients/client";
            NodeList client_nodes = (NodeList) xPath.compile(client_expr).evaluate(document, XPathConstants.NODESET);

            clientPortMap = new HashMap<Integer, Integer>();
            for (int i = 0; i < client_nodes.getLength(); i++) {
                Node node = client_nodes.item(i);
                Integer client_num = Integer.parseInt(node.getAttributes().getNamedItem("id").getNodeValue());
                Integer client_udp_port = Integer.parseInt(xPath.compile("./udp_in").evaluate(node));
                clientPortMap.put(client_num, client_udp_port);
            }
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
    }

    public Integer getSuccessor() {
        return successor;
    }

    public Integer getPredecessor() {
        return predecessor;
    }

}