package config;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class MasterConfig {
    public int wakeUpIntervalMs;
    public int serverPingIntervalMs;
    public int udpIn;

    public boolean crashPred;
    public boolean crashSucc;
    public boolean crashTailExtend;
    public boolean crashNodeExtend;


    public MasterConfig(String config_file) {
        Document document;
        try {
            DocumentBuilder builder = Config.getBuilder();
            document = builder.parse(new FileInputStream(config_file));
            XPath xPath = XPathFactory.newInstance().newXPath();

            String wakeupIntervalExpr = "/xml/master/wakeup_interval";
            String serverPingIntervalExpr = "/xml/master/server_ping_interval";
            String udpInExpr = "/xml/master/udp_in";

            String predCrashExpr = "/xml/master/crash_pred";
            String succCrashExpr = "/xml/master/crash_succ";
            String crashTailExtendExpr = "/xml/master/crash_tail_extend";
            String crashNodeExtendExpr = "/xml/master/crash_node_extend";

            String wakeupInterval = xPath.compile(wakeupIntervalExpr).evaluate(document);
            this.wakeUpIntervalMs = Integer.parseInt(wakeupInterval) * 1000;
            String serverPingInterval = xPath.compile(serverPingIntervalExpr).evaluate(document);
            this.serverPingIntervalMs = Integer.parseInt(serverPingInterval) * 1000;
            String udpIn = xPath.compile(udpInExpr).evaluate(document);
            this.udpIn = Integer.parseInt(udpIn);

            String predCrash = xPath.compile(predCrashExpr).evaluate(document);
            String succCrash = xPath.compile(succCrashExpr).evaluate(document);
            String crashTail = xPath.compile(crashTailExtendExpr).evaluate(document);
            String crashNode = xPath.compile(crashNodeExtendExpr).evaluate(document);

            if (predCrash != null)
                this.crashPred = Boolean.parseBoolean(predCrash);
            if (predCrash != null)
                this.crashSucc = Boolean.parseBoolean(succCrash);
            if (predCrash != null)
                this.crashTailExtend = Boolean.parseBoolean(crashTail);
            if (predCrash != null)
                this.crashNodeExtend = Boolean.parseBoolean(crashNode);

        } catch (SAXException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "MasterConfig{" +
                "wakeUpIntervalMs=" + wakeUpIntervalMs +
                ", serverPingIntervalMs=" + serverPingIntervalMs +
                ", udpIn=" + udpIn +
                '}';
    }
}