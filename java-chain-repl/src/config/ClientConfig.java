package config;


import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import server.ServerInfo;
import util.Request;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class ClientConfig {
    public int timeout;
    public int resend_count;
    public int udp_in;
    public int failue_tcp_in;
    public List<Request> requests;
    public Map<Integer, Integer> head_map; // TODO: Value should be a pair <head_ip, head_port>
    public Map<Integer, Integer> tail_map; // TODO: Value should be a pair <head_ip, head_port>

    public Map<Integer, ClientConfig> clientConfigMap;

    public ClientConfig(String configFile) {
        clientConfigMap = new HashMap<Integer, ClientConfig>();
        try {
            DocumentBuilder builder = Config.getBuilder();
            Document document;
            document = builder.parse(new FileInputStream(configFile));

            XPath xPath = XPathFactory.newInstance().newXPath();

            String allClientsExpression = "/xml/clients/client/@id";
            NodeList nodeList = (NodeList) xPath.compile(allClientsExpression).evaluate(document, XPathConstants.NODESET);
            for (int i = 0; i < nodeList.getLength(); i++) {
                Integer clientId = Integer.parseInt(nodeList.item(i).getNodeValue());
                ClientConfig clientConfig = new ClientConfig(configFile, clientId);
                clientConfigMap.put(clientId, clientConfig);
            }


        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

    }

    private Integer getRandAccountNum() {
        Integer min = 100;
        Integer max = 200;

        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    private void parseRandRequests(NodeList nodeList, Integer client_id, Integer numRequests) {


        Integer runningSeq = 0;
        while (numRequests > runningSeq) {
            Integer account_num = getRandAccountNum();

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                NamedNodeMap attrs = node.getAttributes();
                Integer numReqOp = Integer.parseInt(attrs.getNamedItem("num_reqs").getNodeValue());

                while (numReqOp-- > 0) {
                    Request request = new Request();
                    request.operation = attrs.getNamedItem("type").getNodeValue();
                    Node amount_node = attrs.getNamedItem("amount"); // TODO: Random amount
                    if (amount_node != null) {
                        request.amount = Integer.parseInt(amount_node.getNodeValue());
                    }
                    String bank = attrs.getNamedItem("bank").getNodeValue();
                    String sequence = (++runningSeq) + "";

                    request.account_number = account_num;
                    request.bank_num = Integer.parseInt(bank);
                    request.client_id = client_id;

                    request.setReq_id(client_id + "." + bank + "." + sequence);
                    requests.add(request);
                }
            }
        }

    }

    private void parseListRequests(NodeList nodeList, Integer client_id) {
        for (int i = 0; i < nodeList.getLength(); i++) {
            Request request = new Request();
            Node node = nodeList.item(i);
            NamedNodeMap attrs = node.getAttributes();
            request.operation = attrs.getNamedItem("type").getNodeValue();
            Node amount_node = attrs.getNamedItem("amount");
            if (amount_node != null) {
                request.amount = Integer.parseInt(amount_node.getNodeValue());
            }
            String bank = attrs.getNamedItem("bank").getNodeValue();
            String sequence = attrs.getNamedItem("seq").getNodeValue();

            request.account_number = Integer.parseInt(attrs.getNamedItem("account").getNodeValue());
            request.bank_num = Integer.parseInt(bank);
            request.client_id = client_id;

            request.setReq_id(client_id + "." + bank + "." + sequence);
            requests.add(request);
        }
    }

    private void parseRequests(XPath xPath, Document document, int client_id) throws XPathExpressionException {
        String req_op_expr = "/xml/clients/client[@id=" + client_id + "]/reqs/op";
        String reqrand_op_expr = "/xml/clients/client[@id=" + client_id + "]/reqs_rand/op_rand";
        String numreq_expr = "/xml/clients/client[@id=" + client_id + "]/reqs_rand/num_requests";
        NodeList nodeListReq = (NodeList) xPath.compile(req_op_expr).evaluate(document, XPathConstants.NODESET);
        NodeList nodeListReqRand = (NodeList) xPath.compile(reqrand_op_expr).evaluate(document, XPathConstants.NODESET);
        if (nodeListReq.getLength() != 0) {
            parseListRequests(nodeListReq, client_id);
        } else {
            Integer numReqs = Integer.parseInt(xPath.compile(numreq_expr).evaluate(document));
            parseRandRequests(nodeListReqRand, client_id, numReqs);
        }


    }

    private void parseHead(XPath xPath, Document document, int client_id) throws XPathExpressionException {

        String server_expr = "/xml/servers";

        NodeList nodes = (NodeList) xPath.compile(server_expr).evaluate(document, XPathConstants.NODESET);
        for (int i = 0; i < nodes.getLength(); i++) {
            Node serverNode = nodes.item(i);
            int bankNum = Integer.parseInt(serverNode.getAttributes().getNamedItem("bank").getNodeValue());
            String port_expr = "./server[@pred=\"-1\"]/udp_in";
            Integer port = Integer.parseInt(xPath.compile(port_expr).evaluate(serverNode));
            head_map.put(bankNum, port);
        }

    }

    private void parseTail(XPath xPath, Document document, int client_id) throws XPathExpressionException {

        String server_expr = "/xml/servers";

        NodeList nodes = (NodeList) xPath.compile(server_expr).evaluate(document, XPathConstants.NODESET);
        for (int i = 0; i < nodes.getLength(); i++) {
            Node serverNode = nodes.item(i);
            int bankNum = Integer.parseInt(serverNode.getAttributes().getNamedItem("bank").getNodeValue());
            String port_expr = "./server[@succ=\"-1\"]/udp_in";
            Integer port = Integer.parseInt(xPath.compile(port_expr).evaluate(serverNode));
            tail_map.put(bankNum, port);
        }

    }


    public ClientConfig(String config_file, int client_id) {
        requests = new ArrayList<Request>();
        head_map = new ConcurrentHashMap<Integer, Integer>(new HashMap<Integer, Integer>());
        tail_map = new ConcurrentHashMap<Integer, Integer>(new HashMap<Integer, Integer>());

        try {
            DocumentBuilder builder = Config.getBuilder();
            Document document;
            document = builder.parse(new FileInputStream(config_file));

            XPath xPath = XPathFactory.newInstance().newXPath();
            String timeout_expr = "/xml/clients/timeout";
            String resend_count_expr = "/xml/clients/resend_count";
            String udp_in_expr = "/xml/clients/client[@id=" + client_id + "]/udp_in";
            String tcp_in_expr = "/xml/clients/client[@id=" + client_id + "]/failure_tcp_in";

            // Parse the xml data for client
            String timeout_ = xPath.compile(timeout_expr).evaluate(document);
            this.timeout = Integer.parseInt(timeout_);

            String resend_count_ = xPath.compile(resend_count_expr).evaluate(document);
            this.resend_count = Integer.parseInt(resend_count_);

            String udp_in_ = xPath.compile(udp_in_expr).evaluate(document);
            this.udp_in = Integer.parseInt(udp_in_);

            String failure_tcp_in = xPath.compile(tcp_in_expr).evaluate(document);
            this.failue_tcp_in = Integer.parseInt(failure_tcp_in);

            // PARSE REQUETS
            parseRequests(xPath, document, client_id);

            // PARSE SERVER HEAD DATA
            parseHead(xPath, document, client_id);
            // PARSE SERVER TAIL DATA
            parseTail(xPath, document, client_id);

        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "ClientConfig{" +
                "timeout=" + timeout +
                ", resend_count=" + resend_count +
                ", udp_in=" + udp_in +
                ", failue_tcp_in=" + failue_tcp_in +
                ", head_map=" + head_map +
                ", tail_map=" + tail_map +
                ", clientConfigMap=" + clientConfigMap +
                '}';
    }

    public static void main(String[] args) {
        ClientConfig config = new ClientConfig("/home/soujanya/academics/async/chain-replication/java-chain-repl/config/basic-config.xml", 1);
    }
}