package client;

import config.ClientConfig;
import logger.ChainReplicationLogger;
import config.Config;
import util.Reply;
import util.Request;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.List;
import java.util.logging.Level;

/**
 * Thread that sends request to the head of the bank chain via UDP connection.
 */
public class UdpClientSendThread implements Runnable {
    Integer port;
    String config_file;
    int client_id;
    private Client client;


    public UdpClientSendThread(Integer port, Integer id, Client client) {
        this.port = port;
        this.client_id = id;
        this.client = client;
    }


    // Function that encapsulates logi for sending UDP packet from client to server head
    private void send(DatagramSocket socket, ClientConfig clientConfig, Request request) throws IOException {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(bos);
        os.writeObject(request);
        byte[] sendData = bos.toByteArray();

        //prepare send packet to the server head for the given bank
        String dst_ip = "localhost";
        Integer dst_port;
        // Get the destination port for the request. Queries go to tail and updates go to head.
        if(request.getOperation().equals("get_balance")){
            dst_port = clientConfig.tail_map.get(request.bank_num);
        } else {
            dst_port = clientConfig.head_map.get(request.bank_num);
        }

        DatagramPacket sendServerPacket = new DatagramPacket(sendData,
                sendData.length,
                InetAddress.getByName(dst_ip), dst_port);
        //send packet
        System.out.println("\n[ client.Client " + client_id + " ] UDP util.Request sent from client to port :" + dst_port);
        ChainReplicationLogger.myLogger.log(Level.INFO, "["+request+"] sent to server");
        System.out.println(request);
        socket.send(sendServerPacket);
    }

    /**
     * Method that implements wait till the client gets a response or till timer expires. If the response
     * is not received before timwout a NULL value is returned.
     */
    private Reply await(String request_id, int timeout) {
        timeout = timeout * 2; // We wait for 500 ms, hence double timeout value.
        while (timeout > 0) {
            boolean containsKey;
            synchronized (client.replyMap) {
                containsKey = client.replyMap.containsKey(request_id);
            }
            try {
                if (containsKey) {
                    // We found a response, return the reply and remove it from the queue
                    synchronized (client.replyMap) {
                        return client.replyMap.remove(request_id);
                    }
                }
                // Sleep for sometime before you check if response is available!
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            timeout--;
        }

        System.out.println("TIMED OUT.. :" + timeout);
        return null;
    }

    @Override
    public void run() {

        try {
            DatagramSocket socket = new DatagramSocket(port);
            ClientConfig clientConfig = client.clientConfig;
            List<Request> requests = clientConfig.requests;

            // Send requests one after the other.
            while(true) { // TODO: Replace
                for (Request request : requests) {
                    Thread.sleep(1000);
                    // int resend_count = clientConfig.resend_count;
                    int resend_count = 2;

                    // Retry logic
                    while (resend_count > 0) {
                        send(socket, clientConfig, request);
                        // Wait for response to be received.
                        Reply reply = await(request.req_id, clientConfig.timeout);
                        // Break resend and send next request if a valid reply was received
                        if (reply != null) {
                            break;
                        }
                        // Retry if reply was null
                        ChainReplicationLogger.myLogger.log(Level.WARNING, "Timeout occured util.Request [" + request.req_id + "] resending to server");
                        System.out.println("RESENDING..");
                        resend_count--;
                    }
                }
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}

