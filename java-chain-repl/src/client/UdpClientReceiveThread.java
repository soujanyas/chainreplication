package client;

import config.ClientConfig;
import logger.ChainReplicationLogger;
import config.Config;
import util.Reply;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.logging.Level;

// Thread that keeps listening to requests from predecessor
public class UdpClientReceiveThread implements Runnable {
    int client_id;
    Client client;

    // DO: Socket predSocket
    int count = 0;

    public UdpClientReceiveThread(int client_id, Client client) {
        this.client_id = client_id;
        this.client = client;
    }

    /**
     * Thread that waits for the reponse to be received on the UDP incoming socket.
     */
    private Reply receive(DatagramSocket receiveServerSocket, Integer port) throws IOException, ClassNotFoundException {
        // initialize bytes array for storing sending and receiving data
        byte[] receiveData = new byte[1024];

        // create a receive packet
        DatagramPacket receiveClientPacket = new DatagramPacket(receiveData, receiveData.length);
        //actually receive data
        receiveServerSocket.receive(receiveClientPacket);
        //convert bytes to data

        byte[] receivedDataAsObject = receiveClientPacket.getData();
        ByteArrayInputStream in = new ByteArrayInputStream(receivedDataAsObject);
        ObjectInputStream is = new ObjectInputStream(in);
        Reply rep = (Reply) is.readObject();
        ChainReplicationLogger.myLogger.log(Level.INFO, "["+rep+"]");
        System.out.println("[ client.Client " + client_id + " ] UDP Response Received at client on port :" + port);
        System.out.println(rep);
        return rep;
    }

    @Override
    public void run() {
        DatagramSocket receiveServerSocket = null;
        ClientConfig clientConfig = client.clientConfig;

        try {
            int port = clientConfig.udp_in;
            receiveServerSocket = new DatagramSocket(port);
            while (true) {
                Reply rep = receive(receiveServerSocket, port);
                synchronized (client.replyMap) {
                    client.replyMap.put(rep.request_id, rep);
                }
                Thread.yield();
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
