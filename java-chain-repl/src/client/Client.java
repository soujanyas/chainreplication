package client;

import config.ClientConfig;
import logger.ChainReplicationLogger;
import config.Config;
import util.Reply;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

/**
 * Created by soujanya on 10/11/14.
 *
 */
public class Client {

    final Map<String, Reply> replyMap;
    ClientConfig clientConfig;

    public Client(String configFile, Integer clientId) {
        replyMap = Collections.synchronizedMap(new HashMap<String, Reply>());
        clientConfig = new ClientConfig(configFile, clientId);
    }


    public void run_client(int client_id, String config_file){

        int randPort = 19000 + client_id;

        Thread clientSendThread = new Thread(new UdpClientSendThread(randPort, client_id, this));
        Thread clientRecvThread = new Thread(new UdpClientReceiveThread(client_id, this));
        Thread clientFailureDetectThread = new Thread(new TcpClientFailureDetectThread(this, clientConfig.failue_tcp_in));

        clientSendThread.start();
        clientRecvThread.start();
        clientFailureDetectThread.start();
    }

    private static void help(){
        System.out.println("Usage: java client.Client <client_id> <config_file>");
    }

    public static void main(String[] args){
        if( args.length < 2) {
            help();
            return;
        }
        String file_name;
        int client_id;
        try {
            file_name = args[1];
            client_id = Integer.parseInt(args[0]);
        } catch(Exception e){
            help();
            return;
        }
        String filename = "client.Client."+client_id;
        ChainReplicationLogger chainReplicationLogger = ChainReplicationLogger.getInstance(filename);
        chainReplicationLogger.myLogger.log(Level.INFO, "client.Client"+client_id+" Initialized");
        // RUN CLIENT..
        new Client(file_name, client_id).run_client(client_id, file_name);
    }

}
