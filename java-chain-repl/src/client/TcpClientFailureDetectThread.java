package client;

import logger.ChainReplicationLogger;
import util.Ack;
import util.Constants;
import util.FailureUpdateClientRequest;
import util.ThreadUtils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.logging.Level;

/**
 * Client thread that updates head/tail map for server.
 */
public class TcpClientFailureDetectThread implements Runnable {

    Client client;
    Integer port;

    public TcpClientFailureDetectThread(Client client, Integer port) {
        this.client = client;
        this.port = port;
    }

    void handleClientUpdates(FailureUpdateClientRequest request) {
        if (request.updateType.equals(Constants.updateHead)) {
            Map<Integer, Integer> headmap = client.clientConfig.head_map;
            headmap.put(request.bankNum, request.newPort);
            // TODO: Logger
            System.out.println("Client: Updated head HeadMap : " + headmap);
        } else if (request.updateType.equals(Constants.updateTail)) {
            Map<Integer, Integer> tailMap = client.clientConfig.tail_map;
            tailMap.put(request.bankNum, request.newPort);
            // TODO: Logger
            System.out.println("Client: Updated tail HeadMap : " + tailMap);
        } else {
            System.out.println("Invalid client update operation");
        }
    }

    @Override
    public void run() {
        ServerSocket tcpWelcomeSocket = null;
        ObjectInputStream inStream = null;
        boolean reconnect = true;
        while (true) {
            try {

                if (reconnect) {
                    System.out.println("Establishing client failure socket..");
                    reconnect = false;
                    // Port will not change, you just have to start listening for new connection.
                    if (tcpWelcomeSocket != null && !tcpWelcomeSocket.isClosed()) {
                        // Close the existing connection and start a new one
                        tcpWelcomeSocket.close();
                    }
                    tcpWelcomeSocket = new ServerSocket(this.port);
                    Socket connectionSocket = tcpWelcomeSocket.accept();
                    inStream = new ObjectInputStream(connectionSocket.getInputStream());
                }

                // Wait for data available on the input stream
                System.out.println("waiting client failure read..");
                FailureUpdateClientRequest request = (FailureUpdateClientRequest) inStream.readObject();
                System.out.println("Received client requesst:" + request);
                handleClientUpdates(request);
                ChainReplicationLogger.myLogger.log(Level.INFO, "FailureUpdateClientRequest [" + request + "] received on Port :" + port);

                Thread.yield();
            } catch (IOException e) {
                reconnect = true;
                ThreadUtils.closeServerSocket(tcpWelcomeSocket);
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
