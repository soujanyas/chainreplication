package server;

import logger.ChainReplicationLogger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by soujanya on 10/29/14.
 */
public class ServerThread {
    String ip;
    Integer thisPort;
    Integer id;
    boolean alive = true;

    public static Logger logger;
    String threadName;

    public ServerThread(Logger myLogger, String threadName) {
        logger = myLogger;
        ArrayList<String> tnameList = new ArrayList<String>(Arrays.asList(threadName.split("\\.")));
        this.threadName = "[" + tnameList.get(tnameList.size() - 1) + "] ";
    }

    public ServerThread(String ip, Integer thisPort, Integer id, Logger myLogger, String threadName) {
        this(myLogger, threadName);
        this.ip = ip;
        this.thisPort = thisPort;
        this.id = id;
    }

    public void terminate() {
        Thread.currentThread().interrupt();
        alive = false;
    }
}
