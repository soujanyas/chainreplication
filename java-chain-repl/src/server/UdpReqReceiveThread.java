package server;

import logger.ChainReplicationLogger;
import util.Ack;
import util.Constants;
import util.Reply;
import util.Request;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.logging.Level;

/**
 * Created by soujanya on 10/29/14.
 */
// Thread that keeps listening to requests from predecessor
public class UdpReqReceiveThread extends ServerThread implements Runnable {
    private Server server;
    private Integer udpReceivedMessageCount = 0;

    public UdpReqReceiveThread(Server server, String ip, Integer port, Integer id) {
        super(ip, port, id, ChainReplicationLogger.myLogger, UdpReqReceiveThread.class.getName());
        this.server = server;
    }

    private boolean emulateMessageLoss() {
        if (server.serverConfig.messageLoss != 0) {
            Integer numRequestRange = 100 / server.serverConfig.messageLoss;
            udpReceivedMessageCount++;
            if (udpReceivedMessageCount % numRequestRange == 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void run() {
        DatagramSocket receiveServerSocket;
        try {
            receiveServerSocket = new DatagramSocket(thisPort);
            while (!Thread.currentThread().isInterrupted()) {

                // initialize bytes array for storing sending and receiving data
                byte[] receiveData = new byte[1024];
                DatagramPacket receiveClientPacket = new DatagramPacket(receiveData, receiveData.length);
                receiveServerSocket.receive(receiveClientPacket);

                byte[] receivedDataAsObject = receiveClientPacket.getData();
                ByteArrayInputStream in = new ByteArrayInputStream(receivedDataAsObject);
                ObjectInputStream is = new ObjectInputStream(in);
                Request req = (Request) is.readObject();

                logger.log(Level.INFO, threadName + "################################################");
                if (emulateMessageLoss()) {
                    logger.log(Level.INFO, threadName + "Request dropped in client->server:" + req);
                    continue;
                }

                logger.log(Level.INFO, threadName + "Request [" + req.req_id + "] received for processing");
                // Handle the request
                Reply rep = server.handle_op(req);
                server.updateMessageCount(Constants.receiveShutdown);

                // Do a TCP request forward or UDP response forward based on what type of node it is!
                if (server.isTail()) {
                    System.out.println( "UDP: util.Request ["+req.req_id+"] processed at tail");
                    logger.log(Level.INFO, threadName + "UDP: util.Request [" + req.req_id + "] processed at tail");
                    server.udpEvents.add(rep);
                    server.handleAck(new Ack(req.account_number, req.req_id));
                } else {
                    System.out.println( "UDP: util.Request ["+req.req_id+"] processed");
                    logger.log(Level.INFO, threadName + "UDP: util.Request [" + req.req_id + "] processed");
                    server.tcpEvents.add(req);
                }
                Thread.yield();
            }

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Thread terminating.." + threadName);
        logger.log(Level.INFO, "Thread terminating.." + threadName);
    }
}
