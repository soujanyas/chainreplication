package server;

import logger.ChainReplicationLogger;
import util.Ack;
import util.Request;
import util.ThreadUtils;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.logging.Level;

/**
 * Thread for sending ack to the predecessor.
 */
public class TcpAckSendThread extends ServerThread implements Runnable {

    Server server;
    Integer port;
    boolean alive = true;

    public TcpAckSendThread(Server server, Integer port) {
        super(ChainReplicationLogger.myLogger, TcpAckSendThread.class.getName());
        this.server = server;
        this.port = port;
    }

    @Override
    public void run() {

        Socket tcpSocket = null;
        ObjectOutputStream objectOutputStream = null;
        while (!Thread.currentThread().isInterrupted()) {
            try {
                // Reinitialize a new socket if successor got changed.
                //TODO Thread protect succUpdated
                if (server.predUpdatedAck || (tcpSocket != null && tcpSocket.isClosed())) {
                    if (tcpSocket != null && !tcpSocket.isClosed()) {
                        tcpSocket.close();
                    }
                    boolean notConnected = true;

                    // There is a cyclic server initialization dependency for ack and tcprequest threads.
                    // Hence waiting till ack server gets started in the predecessor.
                    while (notConnected) {
                        try {
                            if (server.predUpdatedAck) {
                                // Reconnect with the new predecessor port
                                this.port = server.predAckPort;
                            }
                            tcpSocket = new Socket("localhost", this.port);
                        } catch (ConnectException e) {
                            if (!server.isHead()) { // Reduce some noise debugs
                                System.out.println("Waiting on ack server..");
                            }
                            Thread.yield();
                            Thread.sleep(1000);
                            continue;
                        }
                        // Exit if connection successful
                        notConnected = false;
                        if (server.predUpdatedAck) {
                            server.predUpdatedAck = false;
                        }
                    }
                    objectOutputStream = new ObjectOutputStream(tcpSocket.getOutputStream());
                }

                // Wait till there are any messages in the queue
                while (server.ackEvents.size() == 0 && !Thread.currentThread().isInterrupted()) {
                    // yield to the Recv thread
                    Thread.yield();
                }
                if (Thread.currentThread().isInterrupted()) {
                    break;
                }
                Ack ack = server.ackEvents.remove(0);

                // Thread.sleep(2000); // Acknowledgment delay
                objectOutputStream.writeObject(ack);
                objectOutputStream.flush();
                logger.log(Level.INFO, threadName + "Ack [" + ack + "] sent to succ port:" + port + " for processing");

            } catch (IOException e) {
                ThreadUtils.closeClientSocket(tcpSocket);
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Thread terminating.." + threadName);
        logger.log(Level.INFO, "Thread terminating.." + threadName);
        ThreadUtils.closeClientSocket(tcpSocket);
    }
}
