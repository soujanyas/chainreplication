package server;

import logger.ChainReplicationLogger;
import util.Ack;
import util.Reply;
import util.Request;
import util.ThreadUtils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;

/**
 * Thread for receiving ack from the successor.
 */
public class TcpAckReceiveThread extends ServerThread implements Runnable {

    private Server server;
    private Integer thisPort;
    boolean alive = true;

    public TcpAckReceiveThread(Server server, Integer thisPort) {
        super(ChainReplicationLogger.myLogger, TcpAckReceiveThread.class.getName());
        this.server = server;
        this.thisPort = thisPort;
    }

    @Override
    public void run() {
        ServerSocket tcpWelcomeSocket = null;
        ObjectInputStream inStream = null;

        while (!Thread.currentThread().isInterrupted()) {
            try {
                if (server.succUpdatedAck || (tcpWelcomeSocket != null && tcpWelcomeSocket.isClosed())) {
                    server.succUpdatedAck = false;
                    // Port will not change, you just have to start listening for new connection.
                    if (tcpWelcomeSocket != null && !tcpWelcomeSocket.isClosed()) {
                        // Close the existing connection and start a new one
                        tcpWelcomeSocket.close();
                    }
                    tcpWelcomeSocket = new ServerSocket(this.thisPort);
                    Socket connectionSocket = tcpWelcomeSocket.accept();
                    inStream = new ObjectInputStream(connectionSocket.getInputStream());
                }

                // Wait for data available on the input stream
                Ack ack = (Ack) inStream.readObject();
                logger.log(Level.INFO, threadName + " :: Ack [" + ack + "] received on Port :" + thisPort);

                server.handleAck(ack);
                Thread.yield();

            } catch (IOException e) {
                // e.printStackTrace();
                ThreadUtils.closeServerSocket(tcpWelcomeSocket);
                System.out.println("Found an exception in ack receive thread, trying to reconnect..");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Thread terminating.." + threadName);
        logger.log(Level.INFO, "Thread terminating.." + threadName);
        ThreadUtils.closeServerSocket(tcpWelcomeSocket);
    }
}
