package server;

import config.Config;
import config.MasterConfig;
import logger.ChainReplicationLogger;
import util.Reply;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.logging.Level;

/**
 * Class for sending UDP keep alive messages to master.
 */
public class UdpKeepAliveSendThread extends ServerThread implements Runnable{
    private MasterConfig masterConfig;
    private Server server;

    public UdpKeepAliveSendThread(Server server, String config_file, String ip, Integer thisPort, Integer id){
        super(ip, thisPort, id, ChainReplicationLogger.myLogger, UdpKeepAliveSendThread.class.getName());
        this.server = server;
        masterConfig = new MasterConfig(config_file);
    }

    @Override
    public void run() {
        try {
            DatagramSocket thisServerSocket = new DatagramSocket(thisPort);
            int serverPingIntervalMs = masterConfig.serverPingIntervalMs;

            while (!Thread.currentThread().isInterrupted()) {
                Thread.sleep(serverPingIntervalMs);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream os = new ObjectOutputStream(bos);

                os.writeObject(server.serverInfo);
                byte[] sendData = bos.toByteArray();

                String dst_ip = "localhost"; // TODO: Replace
                Integer dst_port = masterConfig.udpIn;
                //prepare send packet
                DatagramPacket dstServerPacket = new DatagramPacket(sendData,
                        sendData.length,
                        InetAddress.getByName(dst_ip),
                        dst_port);
                //send packet
                //ChainReplicationLogger.myLogger.log(Level.INFO, "Keep alive sent to master at port:" + dst_port);
                thisServerSocket.send(dstServerPacket);
                if (server.serverInfo.specialFlag != null) {
                    // Reset special flag
                    System.out.println("Special flag reset..");
                    server.serverInfo.specialFlag = null;
                }
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {

            //e.printStackTrace();
        }
        logger.log(Level.INFO, "Thread terminating.." + threadName);
        System.out.println("Thread terminating.." + threadName);
    }
}
