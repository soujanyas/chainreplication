package server;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

import config.ServerConfig;
import logger.ChainReplicationLogger;
import util.*;

import javax.security.auth.callback.CallbackHandler;

public class Server {

    List<Request> tcpEvents;
    List<Reply> udpEvents;
    Integer serverId;
    Integer msgCount = 0;

    List<Ack> ackEvents;
    // NOTE: Read only fields.
    public Integer predecessor;
    public Integer successor;
    Map<Integer, Account> accountdetails;
    Map<String, Map.Entry<Request, Reply>> sent;
    ServerInfo serverInfo;
    ServerConfig serverConfig;

    /* Booleans used for communicating the successor/predecessor updation.*/
    public boolean predUpdated = true;
    public boolean succUpdated = true;
    public boolean predUpdatedAck = true;
    public boolean succUpdatedAck = true;

    public Integer predTcpPort;
    public Integer succTcpPort;

    public Integer predAckPort;
    public Integer succAckPort;

    public boolean extendInProgress = false;

    public ChainReplicationLogger chainReplicationLogger;

    public Server(int serverId, int bankId, String configFile) {
        accountdetails = new ConcurrentHashMap<Integer, Account>();
        tcpEvents = Collections.synchronizedList(new ArrayList<Request>());
        udpEvents = Collections.synchronizedList(new ArrayList<Reply>());
        ackEvents = Collections.synchronizedList(new ArrayList<Ack>());
        sent = Collections.synchronizedMap(new HashMap<String, Map.Entry<Request, Reply>>());
        this.serverId = serverId;
        serverConfig = new ServerConfig(configFile, serverId, bankId);
        // Update predecessor and successor details.
        predecessor = serverConfig.getPredecessor();
        predTcpPort = serverConfig.reqRecvPort;
        predAckPort = serverConfig.ackSendPort_pred;
        successor = serverConfig.getSuccessor();
        succTcpPort = serverConfig.reqSendPort_succ;
        succAckPort = serverConfig.ackRecvPort;

        this.serverInfo = new ServerInfo(serverId, bankId, serverConfig.tcpFailPort, serverConfig.tcpFailIp);

    }

    public boolean isHead() {
        return predecessor == -1;
    }

    public boolean isTail() {
        return successor == -1;
    }

    public synchronized Reply getBalance(Request req, Map<Integer, Account> accountdetails) {
        Account account = accountdetails.get(req.account_number);
        Map<Request, Reply> hist = account.history;
        Reply rep = new Reply("GetBalance Transaction Successful",
                account.getBalance(),
                req.client_id,
                req.req_id);
        ChainReplicationLogger.myLogger.log(Level.INFO, "GetBalance Transaction Successful, Current Balance is: ["
                + account.getBalance() + "] for request [" + req.req_id + "]");
        hist.put(req, rep);
        return rep;
    }

    public synchronized Reply deposit(Request req, Map<Integer, Account> accountdetails) {
        Account account = accountdetails.get(req.account_number);
        float bal = account.getBalance();
        bal = bal + req.amount;
        account.setBalance(bal);
        Map<Request, Reply> hist = account.history;

        Reply rep = new Reply("Deposit Transaction Successful", account.getBalance(), req.client_id, req.req_id);
        System.out.println("Deposit Transaction Successful" + account.getBalance());
        ChainReplicationLogger.myLogger.log(Level.INFO, "Deposit Transaction Successful, Current Balance is: ["
                + account.getBalance() + "] for request [" + req.req_id + "]");
        hist.put(req, rep);
        return rep;
    }

    public synchronized Reply withdrawal(Request req, Map<Integer, Account> accountdetails) {
        Account account = accountdetails.get(req.account_number);
        float bal = account.getBalance();
        //check for sufficient balance
        if (req.getAmount() <= bal) {
            bal = bal - req.amount;
            account.setBalance(bal);
            Map<Request, Reply> hist = account.history;

            Reply rep = new Reply("Withdrawal Transaction Successful", account.getBalance(), req.client_id, req.req_id);
            ChainReplicationLogger.myLogger.log(Level.INFO, "Withdrawal Transaction Successful, Current Balance is: ["
                    + account.getBalance() + "] for request [" + req.req_id + "]");
            hist.put(req, rep);
            return rep;
        } else {
            Map<Request, Reply> hist = account.history;
            Reply rep = new Reply("Insufficient Balance", account.getBalance(), req.client_id, req.req_id);
            //need to change logging level
            ChainReplicationLogger.myLogger.log(Level.WARNING, "Insufficient Balance, Transaction unsuccessful, Current Balance is: ["
                    + account.getBalance() + "] for request [" + req.req_id + "]");
            hist.put(req, rep);
            return rep;
        }
    }

    public synchronized void handleAck(Ack ack) {
        // TODO: Acknowledge everything till then.
        ChainReplicationLogger.myLogger.log(Level.INFO, "Sent:" + sent);
        sent.remove(ack.account_number + "." + ack.req_id);
        ackEvents.add(ack);
    }

    public void updateMessageCount(String eventType) {
        if (eventType.equals(serverConfig.shutdownType)) {
            System.out.println("Shutting down msg count updated..");
            msgCount++;
        }

    }

    public synchronized Reply handle_op(Request req) {

        if (req.operation.equals(Constants.operationSyncAccounts)) {
            System.out.println("Adding new account " + req.account_number);
            accountdetails.put(req.account_number, req.account);
            return null;
        } else if (req.operation.equals(Constants.operationSyncAccountsDone)) {
            System.out.println("Syncing of accounts done");
            serverInfo.specialFlag = Constants.operationSyncAccountsDone;
            return null;
        } else {
            // Wait here when extend is in progress. We do not want to process anything during this period.
            while (extendInProgress) {
                int i = 0;
                while (i++ % 50 == 0) {
                    //System.out.println("Server chain extension in progress");
                    ChainReplicationLogger.myLogger.log(Level.INFO, "Extend in progress..");
                }
                Thread.yield();
            }
        }
        // Check if account exists if not create one
        if (!accountdetails.containsKey(req.getAccount_number())) {
            //util.Reply rep = new util.Reply();
            Map<Request, Reply> temp = new HashMap<Request, Reply>();
            Account acct = new Account(req.getAccount_number(), 0, temp);
            ChainReplicationLogger.myLogger.log(Level.INFO, "util.Account [" + req.account_number + "] Created");
            accountdetails.put(req.getAccount_number(), acct);
        }

        //actual business logic


        //Check for duplicate
        Account account = accountdetails.get(req.account_number);

        Map<Request, Reply> hist = account.history;

        Set<Map.Entry<Request, Reply>> entrySet = hist.entrySet();
        Iterator<Map.Entry<Request, Reply>> itr = entrySet.iterator();
        while (itr.hasNext()) {
            Map.Entry<Request, Reply> entry = itr.next();
            Request tempReq = entry.getKey();
            if (tempReq.getReq_id().equalsIgnoreCase(req.getReq_id()))
            // it means duplicate exists
            {
                if (req.getAccount_number() != tempReq.getAccount_number()
                        || req.getAmount() != tempReq.getAmount()
                        || !req.getOperation().equals(tempReq.getOperation())) {
                    // Inconsistent History
                    // nothing to do tail will automatically reply the message as Inconsistant with history
                    // so we just pass the req object to next server
                    ChainReplicationLogger.myLogger.log(Level.SEVERE,
                            "Request Inconsistent with history for request id [" + req.req_id + "]");
                    Reply rep = entry.getValue();
                    return new Reply("Inconsistent with history", rep.balance, req.client_id, req.req_id);
                } else {
                    // If duplicate return the old reply and do not process it again!
                    ChainReplicationLogger.myLogger.log(Level.SEVERE, "Duplicate request for request id [" + req.req_id + "] exists");
                    assert entry.getValue() != null;
                    Reply rep = entry.getValue();
                    rep.status = rep.status + " DUP "; // MARK AS DUPLICATE
                    return rep;
                }
                // Only duplicate so here also just pass the req and tail will reply the message accordingly
            }
        }

        Reply rep = null;
        if (req.getOperation().equals("get_balance")) {
            rep = getBalance(req, accountdetails);
        } else if (req.getOperation().equals("deposit")) {
            rep = deposit(req, accountdetails);
        } else if (req.getOperation().equals("withdraw")) {
            rep = withdrawal(req, accountdetails);
        } else {
            return new Reply("Invalid operation", 0, req.client_id, req.req_id);
        }

        if (!req.getOperation().equals("get_balance")) {
            // Add request to sent list.
            Map.Entry<Request, Reply> sentEntry = new AbstractMap.SimpleEntry<Request, Reply>(req, rep);
            sent.put(req.account_number + "." + req.req_id, sentEntry);
        }
        return rep;
    }


    public void waitTillKillSelf(List<Thread> threadList) {

        // Terminating condition for the main thread
        while (this.msgCount < serverConfig.shutdownCount) {
            Thread.yield();
        }

        System.out.println("Msg count reached :" + this.msgCount + " | Shutting down the server : " + serverId);
        if (isHead()) {
            System.out.println("Head Terminating..");
        } else if (isTail()) {
            System.out.println("Tail Terminating..");
        } else {
            System.out.println("Intermediate server terminating..");
        }

        for (Thread thread : threadList) {
            //TODO: Deprecated
            thread.interrupt();
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // Initialize and start all the server threads.
    public void runServer(String configFile, ChainReplicationLogger logger) {

        int randPort = 8000 + serverInfo.serverId + 10 * serverInfo.bankId;
        int keepAliveSendPort = 9000 + serverInfo.serverId + 10 * serverInfo.bankId;

        chainReplicationLogger = logger;

        List<Thread> threadList = new ArrayList<Thread>();
        Thread receiveUdpThread = new Thread(new UdpReqReceiveThread(this, serverConfig.predIp, serverConfig.udpIn,
                serverInfo.serverId));
        Thread sendUdpThread = new Thread(new UdpRepSendThread(this, randPort));
        Thread tcpReqSuccThread = new Thread(new TcpReqSendThread(this, serverConfig.succIp, serverConfig.reqSendPort_succ,
                serverInfo.serverId));
        Thread tcpReqPredThread = new Thread(new TcpReqReceiveThread(this, serverConfig.predIp, serverConfig.reqRecvPort,
                serverInfo.serverId));
        Thread udpKeepAliveThread = new Thread(new UdpKeepAliveSendThread(this, configFile, "localhost", keepAliveSendPort,
                serverInfo.serverId));
        Thread tcpFailureDetectThread = new Thread(new TcpFailureDetectThread(this, serverConfig.tcpFailPort));
        Thread tcpAckRecvThread = new Thread(new TcpAckReceiveThread(this, serverConfig.ackRecvPort));
        Thread tcpAckSendThread = new Thread(new TcpAckSendThread(this, serverConfig.ackSendPort_pred));

        threadList.add(tcpAckRecvThread); // Kill ack threads first
        threadList.add(tcpAckSendThread);
        threadList.add(receiveUdpThread);
        threadList.add(sendUdpThread);
        threadList.add(tcpReqSuccThread);
        threadList.add(tcpReqPredThread);
        threadList.add(udpKeepAliveThread);
        threadList.add(tcpFailureDetectThread);


        //  Start all the threads
        for (Thread thread : threadList) {
            thread.start();
        }

        // Kill all the threads on condition.
        waitTillKillSelf(threadList);
    }

    private static void help() {
        System.out.println("Usage: java server.Server <server_num> <bank_num> <config_file>");
    }

    public static void main(String[] args) {
        if (args.length < 3) {
            help();
            return;
        }
        int server_num = Integer.parseInt(args[0]);
        int bank_num = Integer.parseInt(args[1]);
        String config_file = args[2];
        Server server = new Server(server_num, bank_num, config_file);

        String file_name = "server.Server." + bank_num + "." + server_num;
        ChainReplicationLogger chainReplicationLogger = ChainReplicationLogger.getInstance(file_name);
        chainReplicationLogger.myLogger.log(Level.INFO, "Server " + server_num + " Initialized");

        Integer startupDelay = server.serverConfig.startupDelaySec * 1000;
        chainReplicationLogger.myLogger.log(Level.INFO, "Waiting on Startup delay : " + startupDelay);
        try {
            Thread.sleep(startupDelay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        server.runServer(config_file, chainReplicationLogger);
    }
}
