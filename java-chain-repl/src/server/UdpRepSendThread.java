package server;

import config.ServerConfig;
import logger.ChainReplicationLogger;
import util.Constants;
import util.Reply;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.logging.Level;

/**
 * Created by soujanya on 10/29/14.
 */
// Thread that sends request to the successor
public class UdpRepSendThread extends ServerThread implements Runnable {

    Server server;
    ServerConfig serverConfig;
    Integer thisPort;
    Integer id;
    Integer udpSendMessageCount = 0;

    public UdpRepSendThread(Server server,Integer thisPort) {
        super(ChainReplicationLogger.myLogger, UdpRepSendThread.class.getName());
        serverConfig = server.serverConfig;
        this.server = server;
        this.thisPort = thisPort;
        this.id = id;
    }

    private boolean emulateMessageLoss() {

        if (server.serverConfig.messageLoss != 0) {
            udpSendMessageCount++;
            Integer numRequestRange = 100 / server.serverConfig.messageLoss;
            if (udpSendMessageCount % numRequestRange == 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void run() {
        DatagramSocket thisServerSocket = null;

        while (!Thread.currentThread().isInterrupted()) {
            try {
                Reply reply;
                if(thisServerSocket == null || thisServerSocket.isClosed()){
                    thisServerSocket = new DatagramSocket(thisPort);
                }

                // Wait till there are any messages in the queue
                while (server.udpEvents.size() == 0 && !Thread.currentThread().isInterrupted()) {
                    // yield to the Recv thread
                    Thread.yield();
                }
                if (Thread.currentThread().isInterrupted()) {
                    break;
                }
                reply = server.udpEvents.remove(0);

                if (emulateMessageLoss()) {
                    logger.log(Level.INFO, threadName + "Reply dropped in server->client:" + reply);
                    continue;
                }

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream os = new ObjectOutputStream(bos);
                os.writeObject(reply);
                byte[] sendData = bos.toByteArray();

                String dst_ip = "localhost"; // TODO: Replace
                Integer dst_port = serverConfig.clientPortMap.get(reply.client_id);
                //prepare send packet
                DatagramPacket dstServerPacket = new DatagramPacket(sendData,
                        sendData.length,
                        InetAddress.getByName(dst_ip),
                        dst_port);
                //send packet
                System.out.println("UDP Reply [" + reply.request_id + "] sent to port :" + dst_port);
                logger.log(Level.INFO, threadName + "Reply  [" + reply + "] sent to client [" + reply.client_id + "]");
                thisServerSocket.send(dstServerPacket);
                server.updateMessageCount(Constants.sendShutdown);

            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Thread terminating.." + threadName);
        logger.log(Level.INFO, "Thread terminating.." + threadName);
    }
}
