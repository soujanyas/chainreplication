package server;

import logger.ChainReplicationLogger;
import util.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.logging.Level;

/**
 * Class used for updating the server information when a predecessor/successor crashes.
 */
public class TcpFailureDetectThread extends ServerThread implements Runnable {
    Integer tcpFailPort;
    Server server;

    public TcpFailureDetectThread(Server server, Integer tcpFailPort) {
        super(ChainReplicationLogger.myLogger, TcpFailureDetectThread.class.getName());
        this.tcpFailPort = tcpFailPort;
        this.server = server;
    }

    private void handleSuccFailure() {
        for (Map.Entry<Request, Reply> entry : server.sent.values()) {
            Request request = entry.getKey();
            if (server.isTail()) {
                logger.log(Level.INFO, threadName + "SENTCLR: Acknowledging request :" + request);
                server.udpEvents.add(entry.getValue()); // Send reply to client
                server.sent.remove(request.account_number + "." + request.req_id);
                server.ackEvents.add(new Ack(request.account_number, request.req_id)); // Send acks to predecessor
            } else {
                logger.log(Level.INFO, threadName + "SENTCLR: Resending request :" + request);
                server.tcpEvents.add(entry.getKey());
            }
        }
    }

    private void syncAccounts() {
        Map<Integer, Account> accounts = server.accountdetails;
        int i = 0;
        for (Account account : accounts.values()) {
            String requestId = "sync" + (i++);
            Request request = new Request(requestId, Constants.operationSyncAccounts, account.accountNumber, account);
            server.tcpEvents.add(request);
        }

        // Send a marker to successor that it is done sending all the accounts.
        Request syncDoneReq = new Request("syncDone", Constants.operationSyncAccountsDone);
        server.tcpEvents.add(syncDoneReq);

    }

    @Override
    public void run() {
        ServerSocket tcpWelcomeSocket = null;
        ObjectInputStream inStream = null;
        while (!Thread.currentThread().isInterrupted()) {
            try {
                if (tcpWelcomeSocket == null || tcpWelcomeSocket.isClosed()) {
                    tcpWelcomeSocket = new ServerSocket(tcpFailPort);
                    Socket connectionSocket = tcpWelcomeSocket.accept();
                    inStream = new ObjectInputStream(connectionSocket.getInputStream());
                }

                // Wait for data available on the input stream
                FailureUpdateServerRequest request = (FailureUpdateServerRequest) inStream.readObject();
                System.out.println("[ SERVER " + request + " ] Received on thisPort :" + tcpFailPort);
                logger.log(Level.INFO, threadName + "[" + request + " ] Received on thisPort :" + tcpFailPort);
                String updateType = request.updateType;

                // TODO: When exactly to crash?
                if (request.crashServer) {
                    // Crash this server by setting the terminating condition for main thread.
                    logger.log(Level.INFO, threadName + "Got crash instruction from master.. testing only..");
                    server.msgCount = server.serverConfig.shutdownCount;
                    Thread.yield();
                }

                if (updateType.equals(Constants.updatePred) || updateType.equals(Constants.updatePredExtend)) {
                    logger.log(Level.INFO, threadName + "Updating predecessor details");
                    server.predecessor = request.newServer;
                    server.predAckPort = request.newAckPort;
                    server.predUpdated = true;
                    server.predUpdatedAck = true;

                    if (updateType.equals(Constants.updatePredExtend)) {
                        server.successor = -1;
                    }
                } else if (updateType.equals(Constants.updateSucc)) {
                    logger.log(Level.INFO, threadName + "Updating successor details");
                    server.successor = request.newServer;
                    server.succUpdated = true;
                    server.succUpdatedAck = true;
                    server.succTcpPort = request.newPort;
                    // If an update was received from master implies you need to abort extention
                    server.extendInProgress = false;
                    handleSuccFailure();
                } else if (updateType.equals(Constants.updateSuccExtend)) {
                    logger.log(Level.INFO, threadName + "Updating successor details");
                    // Do not update successor id yet.. Only after account sync is done, this should be updated so that
                    // this server continues to be tail till sync is complete
                    server.succUpdated = true;
                    server.succUpdatedAck = true;
                    server.succTcpPort = request.newPort;
                    // No need to call handle Failure detect
                    logger.log(Level.INFO, threadName + "Syncing accounts..");
                    server.extendInProgress = true;
                    syncAccounts();

                } else if (updateType.equals(Constants.extendDone)) {
                    server.successor = request.newServer;
                    server.extendInProgress = false;
                }

                Thread.yield();
            } catch (IOException e) {
                ThreadUtils.closeServerSocket(tcpWelcomeSocket);
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Thread terminating.." + threadName);
        logger.log(Level.INFO, "Thread terminating.." + threadName);
        ThreadUtils.closeServerSocket(tcpWelcomeSocket);
    }
}
