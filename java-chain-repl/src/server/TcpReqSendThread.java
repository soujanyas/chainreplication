package server;

import logger.ChainReplicationLogger;
import util.Constants;
import util.Request;
import util.ThreadUtils;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.logging.Level;

/**
 *
 * Created by soujanya on 10/29/14.
 */
// Thread that sends request to the successor
public class TcpReqSendThread extends ServerThread implements Runnable {
    private Server server;

    public TcpReqSendThread(Server server, String ip, Integer port, Integer id) {
        super(ip, port, id, ChainReplicationLogger.myLogger, TcpReqSendThread.class.getName());
        this.server = server;
    }

    private void updateMsgCount() {
        if (server.serverConfig.shutdownType.equals(Constants.sendShutdown)) {
            server.msgCount++;
        }
    }

    @Override
    public void run() {

        Socket tcpSocket = null;
        ObjectOutputStream objectOutputStream = null;
        while (!Thread.currentThread().isInterrupted()) {
            try {
                // Reinitialize a new socket if successor got changed or socket was closed!.
                //TODO Thread protect succUpdated
                if (server.succUpdated || ( tcpSocket != null && tcpSocket.isClosed())) {
                    if (tcpSocket != null && !tcpSocket.isClosed()) {
                        tcpSocket.close();
                    }
                    boolean notConnected = true;

                    // Ty connecting till you succeed or a new successor is available
                    while (notConnected) {
                        try {
                            this.thisPort = server.succTcpPort;
                            if(server.succUpdated) {
                                if(!server.isTail()) { // Reduce some noise
                                    System.out.println("[Req Send Thread] Reestablishing connection to port " + server.succTcpPort);
                                    logger.log(Level.INFO, threadName + "Reestablishing connection to port " + server.succTcpPort);
                                }
                            }
                            tcpSocket = new Socket("localhost", this.thisPort);
                        } catch (ConnectException e) {
                            if(!server.isTail()) { // Reduce some noise
                                System.out.println("Waiting on tcp recv server..");
                                logger.log(Level.INFO, threadName + "Waiting on tcp recv server..");
                            }
                            Thread.yield();
                            Thread.sleep(1000);
                            continue;
                        }
                        // Exit if connection successful
                        notConnected = false;
                        if(server.succUpdated ) { // Update this only if it was changed..
                            server.succUpdated = false;
                        }
                    }

                    objectOutputStream = new ObjectOutputStream(tcpSocket.getOutputStream());
                }

                // Wait till there are any messages in the queue
                while (server.tcpEvents.size() == 0 && !Thread.currentThread().isInterrupted()) {
                    // yield to the Recv thread
                    Thread.yield();
                }
                if (Thread.currentThread().isInterrupted()) {
                    break;
                }
                Request req = server.tcpEvents.remove(0);

                objectOutputStream.writeObject(req);
                objectOutputStream.flush();
                server.updateMessageCount(Constants.sendShutdown);
                logger.log(Level.INFO, threadName + "util.Request [" + req.req_id + "] sent to succ for processing");

            } catch (IOException e) {
                ThreadUtils.closeClientSocket(tcpSocket);
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Thread terminating.." + threadName);
        logger.log(Level.INFO, "Thread terminating.." + threadName);
        ThreadUtils.closeClientSocket(tcpSocket);
    }
}
