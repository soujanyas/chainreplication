package server;

import logger.ChainReplicationLogger;
import util.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;

/**
 * Thread class used for receiving server requests over a TCP connection..
 */
// Thread that keeps listening to requests from predecessor
public class TcpReqReceiveThread extends ServerThread implements Runnable {
    private Server server;

    public TcpReqReceiveThread(Server server, String ip, Integer port, Integer id) {
        super(ip, port, id, ChainReplicationLogger.myLogger, TcpReqReceiveThread.class.getName());
        this.server = server;
    }

    @Override
    public void run() {
        ServerSocket tcpWelcomeSocket = null;
        ObjectInputStream inStream = null;

        while (!Thread.currentThread().isInterrupted()) {
            try {
                if (server.predUpdated || (tcpWelcomeSocket != null && tcpWelcomeSocket.isClosed())) {
                    System.out.println("[Req receive]: Reestablishing receive thread on port " + thisPort);
                    // Port will not change, you just have to start listening for new connection.
                    if (tcpWelcomeSocket != null && !tcpWelcomeSocket.isClosed()) {
                        // Close the existing connection and start a new one
                        tcpWelcomeSocket.close();
                    }
                    tcpWelcomeSocket = new ServerSocket(this.thisPort);
                    logger.log(Level.INFO, "[ SERVER " + id + " ] Listening on TCP connection at port:" + thisPort);
                    Socket connectionSocket = tcpWelcomeSocket.accept();
                    logger.log(Level.INFO, "[ SERVER " + id + " ] Accepted TCP connection at port:" + thisPort);
                    inStream = new ObjectInputStream(connectionSocket.getInputStream());
                    server.predUpdated = false;
                }

                // Wait for data available on the input stream
                Request request = (Request) inStream.readObject();
                server.updateMessageCount(Constants.receiveShutdown);
                logger.log(Level.INFO, threadName + "################################################");
                logger.log(Level.INFO, threadName + "Request [" + request.req_id + "] received for processing");
                System.out.println("Request [" + request.req_id + "] received for processing");

                Reply rep = server.handle_op(request);

                if (rep != null) { // reply ca be null for sync requests..
                    if (server.isTail()) { // If tail
                        logger.log(Level.INFO, threadName + "Request processed at Tail");
                        server.udpEvents.add(rep);
                        if (!request.operation.equals("get_balance")) {
                            // Send acknowledgement to predecessor if this is a tail.
                            server.handleAck(new Ack(request.account_number, request.req_id));
                        }
                    } else {
                        logger.log(Level.INFO, threadName + " Request processed at non-tail");
                        server.tcpEvents.add(request);
                    }
                }
                Thread.yield();
            } catch (IOException e) {
                System.out.println("Exception caught in TcpReqRecvThread..");
                ThreadUtils.closeServerSocket(tcpWelcomeSocket);
                // e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Thread terminating.." + threadName);
        logger.log(Level.INFO, "Thread terminating.." + threadName);
        ThreadUtils.closeServerSocket(tcpWelcomeSocket);
    }
}