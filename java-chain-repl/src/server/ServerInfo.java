package server;

import java.io.Serializable;

/**
 * ServerInfo object exchanged between server and Master.
 * Created by Soujanya on 10/30/14.
 */
public class ServerInfo implements Serializable {
    public Integer serverId;
    public Integer bankId;
    public Integer failure_tcp_in;
    public String failure_tcp_ip;

    @Override
    public String toString() {
        return "ServerInfo{" +
                "serverId=" + serverId +
                ", bankId=" + bankId +
                ", failure_tcp_in=" + failure_tcp_in +
                ", failure_tcp_ip='" + failure_tcp_ip + '\'' +
                ", specialFlag='" + specialFlag + '\'' +
                '}';
    }

    public String specialFlag = null;

    public ServerInfo(Integer serverId, Integer bankId, Integer failure_tcp_in, String failure_tcp_ip) {
        this.serverId = serverId;
        this.bankId = bankId;
        this.failure_tcp_in = failure_tcp_in;
        this.failure_tcp_ip = failure_tcp_ip;
    }

}
