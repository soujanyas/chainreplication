#!/bin/bash

set -o verbose

ps -ef| grep "java -ea server.Server" | tr -s ' '| cut -d ' ' -f 2 | xargs kill -9
ps -ef| grep "java -ea client.Client" | tr -s ' '| cut -d ' ' -f 2 | xargs kill -9
ps -ef| grep "java -ea master.Master" | tr -s ' '| cut -d ' ' -f 2 | xargs kill -9

ps -eaf | grep "java -ea server.Server"
ps -eaf | grep "java -ea client.Client"
ps -eaf | grep "java -ea master.Master"
