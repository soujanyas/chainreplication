#!/bin/bash

set -o verbose

SRC_HOME=/home/soujanya/academics/async/chain-replication/java-chain-repl/src
CONFIG_FILE=/home/soujanya/academics/async/chain-replication/java-chain-repl/config/tail_crash.xml

cd $SRC_HOME
javac util/*.java
javac server/*.java
javac client/*.java
javac master/*.java

export CLASSPATH=$SRC_HOME
# FORMAT: java -ea server.Server <server_num> <bank_num> <config_file>
java -ea server.Server 3 1 $CONFIG_FILE &
java -ea server.Server 2 1 $CONFIG_FILE &
java -ea server.Server 1 1 $CONFIG_FILE &

java -ea master.Master $CONFIG_FILE &

java -ea client.Client 1 $CONFIG_FILE &
