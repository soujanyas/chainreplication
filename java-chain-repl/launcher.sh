#!/bin/bash

set -o verbose

PROJECT_HOME=~/asynch/javas-chain-repl-clone/chain/java-chain-repl
SRC_HOME=$PROJECT_HOME/src
CONFIG_FILE=$PROJECT_HOME/config/basic-config.xml

rm -rf $PROJECT_HOME/logs/client/*.log
rm -rf $PROJECT_HOME/logs/server/*.log


cd $SRC_HOME
javac util/*.java
javac server/*.java
javac client/*.java

export CLASSPATH=$SRC_HOME
java -ea server.Server 3 1 $CONFIG_FILE &
java -ea server.Server 2 1 $CONFIG_FILE &
java -ea server.Server 1 1 $CONFIG_FILE &

java -ea server.Server 3 2 $CONFIG_FILE &
java -ea server.Server 2 2 $CONFIG_FILE &
java -ea server.Server 1 2 $CONFIG_FILE &

java -ea client.Client 1 $CONFIG_FILE &
java -ea client.Client 2 $CONFIG_FILE &
