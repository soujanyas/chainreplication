import xml.etree.ElementTree as ET
import sys
import random

def __init__():
  pass


class ClientConfig():

  def __init__(self, filename):

    def list_client_requests(self, requests):
      request_list = list()
      for op in requests.findall("op"):
        request_list.append(dict(op.attrib))
      return request_list

    def list_rand_client_requests(self, requests, client_id, rand_seed):
      random.seed(rand_seed)
      req_list = list()
      running_seq = 0


      total = int(requests.find("num_requests").text)
      while total > 0:
        for request_type in requests.findall("op"):
          operation = request_type.get("type")
          bank = request_type.get("bank")
          amount = request_type.get("amount") if request_type.get("amount") else "0"

          # METADATA
          num_requests = int(request_type.get("num_reqs"))
          dest_bank = request_type.get("dest_bank")
          if request_type.get("reuse_account") == "false":
            start = int(bank) * 100
            end = (int(bank) + 1) * 100 - 1
            src_account = str(random.randint(start, end))

          # For each type of requests, construct the list of REAL requests
          while not num_requests == 0:
            num_requests -= 1
            total -= 1
            running_seq += 1
            req = dict()
            req["bank"] = bank
            req["amount"] = amount
            req["account"] = src_account
            req["operation"] = operation
            req["seq"] = running_seq
            req["type"] = operation
            if dest_bank:
              req["dest_bank"] = dest_bank
              start_dest = int(dest_bank) * 100
              end_dest = (int(dest_bank) + 1) * 100 - 1
              req["dest_account_no"] = str(random.randint(start_dest, end_dest))

            req_list.append(req)

      return req_list



    tree = ET.parse(filename)
    client = tree.find("client")
    self.timeout = int(client.find("timeout").text)
    self.resend_count = int(client.find("resend_count").text)
    self.dyn_topology_up = client.find("dyn_topology_up").text

    self.req_map = dict()
    all_requests = client.findall("reqs")
    for requests in iter(all_requests):
      client_id = int(requests.get("client"))
      type = requests.get("type")
      if(type == "random"):
        seed = int(requests.get("seed"))
        req_list = list_rand_client_requests(self, requests, client_id, seed)
      else:
        req_list = list_client_requests(self, requests)

      self.req_map[client_id] = req_list
      # req_map[requests.get("client")]

  def get_requests(self, client_id):
    if(client_id not in self.req_map):
      print("ERROR: Could not find client config for client %d" % client_id)
      sys.exit(0)
    return self.req_map[client_id]


  def __str__(self):
    return ("timeout: %d  resend_count:%d dyn:%s " % ( self.timeout, self.resend_count, self.dyn_topology_up ))

class ServerConfig():
  def __init__(self, filename, server_id, bank_id):
    tree = ET.parse(filename)
    server = tree.find("server")
    lifetimes = server.findall("lifetime")

    self.type = "unbound"
    self.msg_count = 9999
    self.head_msg_loss_req = 0
    self.tail_msg_loss_req = 0

    head_msg_loss = server.find("head_message_loss_precent")
    tail_msg_loss = server.find("head_message_loss_precent")
    if head_msg_loss is not None:
      self.head_msg_loss_req = 100/int(head_msg_loss.text)
    if tail_msg_loss is not None:
      self.tail_msg_loss_req = 100/int(tail_msg_loss.text)

    for lifetime in iter(lifetimes):
      if(int(lifetime.get("id")) == server_id and int(lifetime.get("bank_id")) == bank_id):
        self.type = lifetime.get("type")
        if(lifetime.get("msg_count") is not None):
          self.msg_count = int(lifetime.get("msg_count"))


  def __str__(self):
    return ("type: %s count:%d" % (self.type, self.msg_count))


class ClusterConfig():
  def __init__(self, filename):
    tree = ET.parse(filename)
    self.num_clients = int(tree.find("num_clients").text)
    self.num_banks = int(tree.find("num_banks").text)
    self.num_servers_chain = int(tree.find("num_servers_chain").text)
    extend = tree.find("extend_server")
    if extend is not None:
      print("### %s" % extend)
      self.extend = True
      self.extend_details = dict()
      self.extend_details["bank_id"] = int(extend.get("bank_id"))
      self.extend_details["server_id"] = int(extend.get("id"))
      self.extend_details["delay"] = int(extend.get("delay"))
    else:
      self.extend = False

  def __str__(self):
    return ("Num clients: %d" % self.num_clients)

class MasterConfig():
  def __init__(self, filename):
    tree = ET.parse(filename)
    master = tree.find("master")

    if(master.find("pred_crash").text == "True"):
      self.pred_crash = True
    else:
      self.pred_crash = False
    if(master.find("succ_crash").text == "True"):
      self.succ_crash = True
    else:
      self.succ_crash = False

    if(master.find("crash_tail_extend").text == "True"):
      self.extend_count = 1
    elif(master.find("crash_node_extend").text == "True"):
      self.extend_count = 2
    else:
      self.extend_count = 5

  def __str__(self):
    return "pred_crash:%s succ_crash:%s extend_count:%s" % \
           (self.pred_crash, self.succ_crash, self.extend_count)
