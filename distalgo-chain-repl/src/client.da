import logging
import time
import os.path
from conf_parser import ClientConfig, ClusterConfig
from data_types import Request,strr

def __init__():
  pass

class Client(process):
  '''
  A Bank client that interacts with server
  '''

  logger = None
  def setup(client_id, filename, head_tail_map):

    self.responses = set()

    def get_operation_type(type):
      if type == "get_balance":
        return "query"
      else:
        return "update"

    def make_requests( conf_requests ):
      '''
      Given a configuration request, construct a valid list of requests that
      can be sent to server for update/query operations.
      '''
      requests = list()
      for conf_req in conf_requests:
        request_id = dict()
        request_id["client_id"] = int(self.client_id)
        request_id["bank"] = int(conf_req["bank"])
        request_id["seq"] = int(conf_req["seq"])
        req = Request()
        req.operation_type = get_operation_type(conf_req["type"])
        req.operation = conf_req["type"]
        req.request_id = request_id
        req.account_no = conf_req["account"]
        req.dest_bank = conf_req["dest_bank"] if "dest_bank" in conf_req else 0
        req.dest_account_no = conf_req["dest_account_no"] if "dest_account_no" in conf_req else "0"
        req.amount = int(conf_req["amount"]) if "amount" in conf_req else 0
        req.updated_balance = 0
        #TODO: Add more values for transfer operation
        requests.append(req)
      return requests

    self.conf = ClientConfig(filename)
    self.requests = make_requests(conf.get_requests(client_id))

    # Create a map to track head and tail for each bank chain
    self.server_info = dict()
    for bank_num, detail in self.head_tail_map.items():
      self.server_info[bank_num] = dict()
      self.server_info[bank_num]["head"] = detail["head"]
      self.server_info[bank_num]["tail"] = detail["tail"]


  def receive(msg=("Replyclient", resp)):
    logger.info(resp)
    resp_id_msg = str(resp.request_id)
    responses.add(resp)

  def receive(msg=("UpdateHeadtoClient", bank, ps), from_=master):
    logger.warning('Client received update head')
    self.server_info[bank]["head"] = ps
    logger.warning('New head is: ' + str(self.server_info[bank]["head"]))

  def receive(msg=("UpdateTailtoClient", bank, ps), from_=master):
    logger.warning('Client received update tail')
    self.server_info[bank]["tail"] = ps
    logger.warning('New tail is: ' + str(self.server_info[bank]["tail"]))

  def initializelogger(logfile):
            logfilepath = '../logs/client/'
            logext = '.log'
            logger = logging.getLogger(logfile)
            logfilepath = '../logs/client/' + logfile + logext
            os.path.isfile(logfilepath)
            if (os.path.exists(logfilepath)):
                os.remove(logfilepath)
            hdlr = logging.FileHandler(logfilepath)
            formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
            hdlr.setFormatter(formatter)
            logger.addHandler(hdlr)
            logger.setLevel(logging.INFO)


  ################ Management handlers to update topology #####################
  def receive(msg=('update_head', bank_num, head), from_=master):
   # logger.debug('Received update_head')

    if bank_num not in self.server_info:
      self.server_info[bank_num] = dict()
    self.server_info[bank_num]["head"] = head

  def receive(msg=('update_tail', bank_num, head), from_=master):
    logger.debug('Received update_tail')
    #output("DEBUG: Received update_tail")

    if bank_num not in self.server_info:
      self.server_info[bank_num] = dict()
    self.server_info[bank_num]["tail"] = head

  def receive(msg=("query_head_tail", bank_num)):
    head = self.server_info[bank_num]["head"]
    tail = self.server_info[bank_num]["tail"]
    output("CLIENT: Query: Head for bank %d is %s" % (bank_num, head))
    output("CLIENT: Query: Tail for bank %d is %s" % (bank_num, tail))


  ############################################################################
  ## MAIN THREAD and supporting functions.. TODO: Make it multi threaded
  #############################################################################

  def pop_request():
    '''
    pop the request from config and return the head applicable for that request.
    '''
    req = self.requests.pop(0)
    bank_num = req.request_id["bank"]
    assert bank_num in self.server_info , "Server info null for bank %d" % bank_num
    head = self.server_info[bank_num]["head"]
    return (head, req)


  def run_client():
    '''
    Client code that sends all requests to the servers till the request list
    is not empty
    '''
    logger.info('Client (%s) initiated' %(self.id))
    timeout_ = self.conf.timeout
    num_req_remaining = len(self.requests)
    while(num_req_remaining > 0):
      #time.sleep(2)
      num_req_remaining -= 1
      n_retries_left = self.conf.resend_count
      (head, req) = pop_request()
      # output("CLIENT: Popped request: %s" % req)

      successful = False
      while not successful and n_retries_left > 0:
        bank_num = req.request_id["bank"]
        if req.operation == 'get_balance':
          server = self.server_info[bank_num]["tail"]
        else:
          server = self.server_info[bank_num]["head"]
          #if req.operation == 'transfer':
        logger.info('Request [' + strr(req.request_id)+ '] for operation ['+ req.operation + '] amount ['+ str(req.amount) + '] for account number ['+req.account_no +'] sent')
        send(("Requestserver", req, self.id), to=server)
        # Wait till timeout and till resp_request_id = request_id
        reply_msg = str(req.request_id)
        # Wait till response with same request id is available
        if await (some(received(("Replyclient",resp)), has = (resp.request_id == req.request_id))):
          logger.info('Response received for Request [' + strr(req.request_id) + ']')
          # logger.info(responses.pop())
          successful = True
        elif timeout(timeout_):
          logger.warning('Timeout response received for Request' + strr(req.request_id) + ' resending the request ')
          output('Timeout response received for Request' + strr(req.request_id) + ' resending the request ')
          output("Await failed retying..")
          n_retries_left -= 1

      #TODO: Test this..
      if n_retries_left == 0:
        logger.error(' Sending Request ' + strr(req.request_id) + ' failed ')
        print("ERROR: Sending request failed.. %s " % req)

  def run():

    # Ensure that client has received all update head and tail requests
    # NA anymore
    output("########### SENDING CLIENT (%d) REQUESTS ###############" % self.client_id)
    initializelogger("client.%d" % self.client_id)
    self.run_client()
    await (some(received(('CLIENT_TERM', _))))
    logger.info('Termination received for Client (%s) exiting client' % self.id)
    output("CLIENT_TERM received.. Exiting client..")



