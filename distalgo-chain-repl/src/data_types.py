
def strr(req_id):
  return ("%d.%d.%d" % (req_id['bank'],  req_id['seq'], req_id['client_id']))

class Request:
  '''
  Request object that will be sent across to the server.
  '''

  def __init__(self, req = None):
    if(req):
      self.operation_type = req.operation_type
      self.operation = req.operation
      #client_id, bank, seq
      self.request_id = req.request_id
      self.account_no = req.account_no
      self.amount = req.amount
      self.dest_bank = req.dest_bank
      self.dest_account_no = req.dest_account_no
      self.updated_balance = req.updated_balance
    else:
      self.operation_type = None
      self.operation = None
      self.request_id = None
      self.account_no = None
      self.amount = None
      self.dest_bank = None
      self.dest_account_no = None
      self.updated_balance = None

  def equals(self, other):
    return ( self.operation_type == other.operation_type and
           self.operation == other.operation and
           self.account_no == other.account_no and
           self.amount == other.amount and
           self.dest_bank == other.dest_bank and
           self.dest_account_no == other.dest_account_no)

  def get_request_id_str(self):
    return "%s [%s.%s.%s]" % (self.operation, self.request_id["bank"],
                              self.request_id["seq"], self.request_id["client_id"])


  def __str__(self):
    return "REQ: %s %s %s %s %s ##" % (self.operation_type, self.operation,
                                strr(self.request_id), self.account_no, self.amount)

class Reply:
  """
  Class for sending reply to the client
  """
  def __init__(self, status, balance, request_id ):
    self.status = status
    self.balance = balance
    self.request_id = request_id

  def __str__(self):
    return "REP: %s %s Balance:%s " % ( strr(self.request_id) ,self.status, self.balance)
