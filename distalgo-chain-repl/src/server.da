import _thread
import logging
import os.path
import time
import copy

from data_types import Reply,strr
from data_types import Request
class Server(process):

    logger = None
    #sentlist = []
    def setup(pred, succ, clients, ps, bank_num, server_num, master,
              failtype, count, typeofserver, severconfig, head_tail_map):
      self.account_details = dict()
      self.sentlist = []
      self.messages = 0
      self.templist = []
      self.msg_loss_msg_count = 0
      self.transferlist = []


# server head receives request from client and server checks if account exists or not, if not then it will create one
# update itself with the details and perform the requested operation and send sync request to sync with other servers
# or ack if its a tail

    def receive(msg=('UpdateHeadTailMapToServers', headtailmap), from_= master):
      self.head_tail_map = headtailmap

    def receive(msg=('Requestserver', req, client_id), from_=_):

      logger.info('New Request ###################################################################################')
      # Emulate message loss
      process_request = True
      head_msg_count = self.severconfig.head_msg_loss_req
      if (head_msg_count is not 0 and self.pred == None):
        self.msg_loss_msg_count += 1
        if (self.msg_loss_msg_count == int(head_msg_count) ):
          self.msg_loss_msg_count = 0
          logger.info('[ Message loss ] Request[ ' + strr(req.request_id) +' ] dropped client->server')
          process_request = False
        else:
          logger.info('[ Message loss ] not happening')

      if(process_request):
        if self.failtype == "received":
          self.messages = self.messages + 1
          time.sleep(0.1)

        logger.info('Request ['+ req.get_request_id_str()+ '] received')
        if self.messages == self.count: # Crash server immediately
          dummy = 0
        else:
          if not req.account_no in self.account_details :
            createaccount(req)
          ack = performoperation(req)
          syncn(req, client_id, ack)

# ack received here is passed on to pred servers so that will be updated

    def receive(msg=('Replyserver', ack, req), from_=self.succ):

      toremove = None
      # Remove from sent list
      for (i, (reqs, client_id)) in enumerate(sentlist):
        if reqs.request_id == req.request_id:
          if (reqs.account_no == req.dest_account_no or reqs.account_no == req.account_no):
              # TRansfer ack will have destination account as
              toremove = i
              break

      if toremove is not None:
        logger.info('Acknowledgement received: for req-id '+ req.get_request_id_str() +', Removing from sent list')
        sentlist.remove(sentlist[toremove])
        logger.info('Updated Sentlist : ' + get_sentlist_str())

      if pred is None and req.operation == "transfer" and self.bank_num == int(req.dest_bank):
        #send to source tail
        sourcetail = self.head_tail_map[req.request_id["bank"]]["tail"]
        logger.info('Acknowledgement sent: for req-id [' + req.get_request_id_str() +'] to predecessor server [' + str(sourcetail) +']' )
        send(('Replyserver', ack, req), to=sourcetail)
      elif pred:
        logger.info('Acknowledgement sent: for req-id [' + req.get_request_id_str() +'] to predecessor server [' + str(self.pred) +']' )
        send(('Replyserver', ack, req), to=self.pred)
      else:
        output(ack)

    def acknowledge(req, client_id, reply):
      client = None
      for c in list(self.clients):
        if c == client_id:
          client = c
          break

      assert client is not None
      if self.failtype == "send":
        self.messages = self.messages + 1

      # Emulate message loss at tail
      tail_msg_count = int(self.severconfig.tail_msg_loss_req)
      if (tail_msg_count is not 0):
        self.msg_loss_msg_count += 1
        if (self.msg_loss_msg_count == int(tail_msg_count) ):
          self.msg_loss_msg_count = 0
          logger.info('[ Message loss ] Request[ ' + strr(req.request_id) +' ] dropped server->client')
        else:
          logger.info('Reply to client sent [' + str(client_id) +']')
          send(('Replyclient', reply), to=client)

      #### TODO: Add During demooo
      else:
        logger.info('Reply to client sent [' + str(client_id) +']')
        send(('Replyclient', reply), to=client)

      if self.failtype == "send":
        self.messages = self.messages + 1

      if self.pred:
        if((req, client_id) in self.sentlist):
            self.sentlist.remove((req, client_id))
        send(('Replyserver', reply, req), to=self.pred)
        logger.info('Acknowledgement sent: for req-id ' + req.get_request_id_str() +
                  ' to predecessor server [' + str(self.pred) +']')


    def send_request(req, client_id, to_server):
      # Increment send message count
      if self.failtype == "send":
        self.messages = self.messages + 1
      logger.info('Request sent to successor server [' + str(to_server) +']')
      logger.info('SentList:' + get_sentlist_str())

      #Send message
      send(('Requestserver', req, client_id), to=to_server)

# syncn() will help traverse between intermediate servers and initiate ack if tail is reached
    def syncn(req, client_id, reply):

      logger.info("Request:" + str(req))
      if succ : #Head or intermediate
        # Add request to sent list
        if((req, client_id) not in self.sentlist):
          self.sentlist.append((req, client_id))
        send_request(req, client_id, self.succ)
      else: # Tail
        # TRANSFER at SRC BANK
        if req.operation == "transfer" and self.bank_num == req.request_id["bank"]:
          if reply.status == "Inconsistent with history" or reply.status == "Insufficient Balance, Transaction Transfer unsuccessful":
            acknowledge(req, client_id, reply)
          else:
            transfer_head = self.head_tail_map[int(req.dest_bank)]["head"]
            #Construct a new request, we don;t want to mess up with history
            new_req = Request(req)
            new_req.updated_balance = reply.balance
            # Swap account numbers..
            new_req.account_no = req.dest_account_no
            new_req.dest_account_no = req.account_no
            # Add old request to sentlist
            if((req, client_id) not in self.sentlist):
              self.sentlist.append((req, client_id))
            send_request(new_req, client_id, transfer_head)
        # DEPOSIT, WITHDRAW, TRANSFER at destination etc
        else:
          acknowledge(req, client_id, reply)

    def receive(msg=('UpdateTailAsIntermediate', bank, ps, extendcount), from_=master):
      # crash the server
      if extendcount == 1:
        self.messages = self.count
      else:
        self.succ = ps
        for k in self.account_details:
          time.sleep(2)
          send(('UpdateAccountDetailsForNewTail', bank, self.account_details[k], k), to=ps)
        logger.info('Pred linked to new server and transferred all the account details to new server')
        send(('SyncDone', self.id), to=ps)

        #send(('SendAccountDetailsDoneNowAckClient', bank ), to=ps)

    def receive(msg=('SyncDone', server), from_=server):
      send(('ExtendDone', self.id), to=master)

    def receive(msg=('UpdateAccountDetailsForNewTail', bank, acctdetail, key), from_=server):
      self.account_details[key] = acctdetail

    def receive(msg=('RetriggerExtend', server, bank), from_=master):
      logger.info('Extend retriggered')
      send(('ExtendChain', bank, self.id), to=master)


    def receive(msg=('UpdatePred', tail, extendcount), from_= master):
      if extendcount == 2:
        logger.info("Extend server failed...")
        self.messages = self.count
      else:
        self.pred = tail
        self.succ = None
        logger.info('Extend server '+str(self.id)+' updated its pred as '+ str(tail))
        #send await signal to master

    def receive(msg=('UpdateHead', ps), from_= master):
      self.pred = None
      logger.info(str(self.id)+' updated as head')


    def receive(msg=('UpdateIntermediateSucc', pspred, pssucc, succcrash), from_= master):

      if succcrash == True:
        self.messages = self.count

      self.pred = pspred
      logger.info("Intermediate server "+str(self.id)+" updated its pred as "+str(pspred))

    def receive(msg=('UpdateIntermediatePred', pspred, pssucc, predcrash), from_= master):

      if predcrash == True:
        self.messages = self.count

      self.succ = pssucc
      logger.info("Intermediate server "+str(self.id)+" updated its succ as "+str(pssucc))
      send(('RequestSentlistToSucc'), to=self.succ)
      #send(('CheckinSentlistAndProcess', self.sentlist), to=self.succ)

    def receive(msg=('RequestSentlistToSucc'), from_=server):
      send(('SentlistSent', self.sentlist), to=self.pred)

    def receive(msg=('SentlistSent', sentlistrecvd), from_=server):

      for k in range(0,len(self.sentlist)-1):

        req = self.sentlist[k][0]
        client = self.sentlist[k][1]
        output(req)

        bool = True
        for k in range(0,len(sentlistrecvd)):
          if req.request_id == sentlistrecvd[k][0].request_id and req.account_no == sentlistrecvd[k][0].account_no:
            bool = False

        if bool == True:
          self.templist.append(self.sentlist[k])
          #store first and then send

      for j in range(0,len(self.templist)):
        req = self.sentlist[k][0]
        client = self.sentlist[k][1]
        send(('RequestServer', req, client), to=self.succ)

    def get_reply(requestobj ): # TODO: Use everywhere
      account = self.account_details[requestobj.account_no]
      (req, rep) = account["history"][str(requestobj.request_id)]
      return rep

    def get_sentlist_str():
      str = ""
      for (req, client_id) in sentlist:
        str = str + req.get_request_id_str() +","
      return str


    def receive(msg=('UpdateTail', ps), from_= master):

      self.succ = None
      logger.warn("Tail for bank %d updated to %s" % (bank_num, ps))
      logger.warn("Tail crash: Handling sentlist:")

      # Avoid concurrent update of sentlist from syncn
      sentlist_deep_copy = []
      for elt in sentlist:
        sentlist_deep_copy.append(elt)

      for (req, client_id) in sentlist_deep_copy:
        rep = get_reply(req)
        syncn(req, client_id, rep)


    ################### TRANSFER #########################

    def receive(msg=('UpdateTailtoServers', bank_num, ps), from_=master):

      if not self.head_tail_map[bank_num]["tail"] == ps:
        self.head_tail_map[bank_num]["tail"] = ps
        logger.info("Tail for bank %d updated to %s" % (bank_num, ps))
      # Head of servers need not transfer acks for transfer operation.


    def receive(msg=('UpdateHeadtoServers', bank_num, ps), from_=master):
      self.head_tail_map[bank_num]["head"] = ps
      logger.warn("Head for bank %d updated to %s" % (bank_num, ps))

      # If a head crashed for transfer operation in progress,
      # resend the request if this is Tail

      # Avoid concurrent update of sentlist from syncn
      sentlist_deep_copy = []
      for elt in sentlist:
        sentlist_deep_copy.append(elt)

      for (req, client_id) in sentlist_deep_copy:
        if(req.operation == "transfer" and self.succ == None and int(req.dest_bank) == int(bank_num)):
          logger.warn("Head crash of other bank: Handling trasnfer in sentlist:")
          # TODO: Only if request is not in sent list of server..
          rep = get_reply( req )
          syncn(req, client_id, rep)


#################################################################################

# performoperation() will perform the actual business logic based on type mentioned in request


    def performoperation(req):
      if req.operation == "deposit":
        dup = check_duplicate(req)
        if dup:
          return dup
        ack = deposit(req)
      elif req.operation == "withdraw":
        dup = check_duplicate(req)
        if dup:
          return dup
        ack = withdrawal(req)
      elif req.operation == "get_balance":
        ack = getbalance(req)
      elif req.operation == "transfer":
        dup = check_duplicate(req)
        if dup:
          return dup
        ack = transfer(req)
      else:
        logger.error('Invalid operation')
        ack = Reply("Invalid operation, Transaction not successful",0, req.request_id)
      return ack


# If account is not present then it creates the account with account number mentioned in req
    def createaccount(req):
      pending = []
      history = dict()
      accounts = {"balance": 0, "pendings": pending, "history": history}
      self.account_details[req.account_no] = accounts
      logger.info('Account ['+ req.account_no +'] created')



# Get balance operation logic
    def getbalance(req):
      acct = self.account_details[req.account_no]
      bal = acct["balance"]
      logger.info('GetBalance operation successful, Current Balance ['+ str(bal) +'] for request ['
                  + strr(req.request_id)+']')
      return Reply("Get balance successful", bal, req.request_id)

    def check_duplicate(req):
      req_id = str(req.request_id)
      account = self.account_details[req.account_no]
      if (req_id in account["history"].keys()):
        # Duplicate
        (req_old, rep_old) = account["history"][req_id]
        if not req_old.equals(req):
          # History not consistent
          logger.warn("Request %s Inconsistent with history" % strr(req.request_id))
          return Reply("Inconsistent with history", rep_old.balance, req.request_id)
        else:
          # Duplicate only, send the old reply
          logger.warn("Request %s is a duplicate" % strr(req.request_id))
          return rep_old
      else:
        # Not a duplicate, return None
        return None


    def update_history(req, rep):
      account = self.account_details[req.account_no]
      account["history"][str(req.request_id)] = (req, rep)


# Deposit operation logic
    def deposit(req):
      acct = self.account_details[req.account_no]
      acct["balance"] = acct["balance"] + req.amount

      logger.info('Deposit operation successful, Current Balance ['+ str(acct["balance"]) +
                  '] for request ['+ strr(req.request_id) +']')
      ack = Reply("Transaction Deposit Successful", acct["balance"], req.request_id)
      update_history(req, ack)
      return ack


# Withdrawal operation logic
    def withdrawal(req):
      acct = self.account_details[req.account_no]
      if (req.amount <= acct["balance"]):
        acct["balance"] = acct["balance"] - req.amount
        logger.info('Withdrawal operation successful, Current Balance ['+ str(acct["balance"]) +
                  '] for request ['+ strr(req.request_id) + ']')
        ack = Reply("Transaction Withdrawal Successful", acct["balance"],req.request_id)
      else:
        logger.error(' Insufficient Balance: Operation Unsuccessful - Current Balance ['+ str(acct["balance"]) +
                  '] for request ['+ strr(req.request_id) +']')
        ack = Reply("Insufficient Balance, Transaction Withdrawal unsuccessful",acct["balance"], req.request_id)
      update_history(req, ack)
      return ack


# Transfer Operation
    def transfer(req):
      acct = self.account_details[req.account_no]
      if req.request_id["bank"] == self.bank_num:
        if (req.amount <= acct["balance"]):
          acct["balance"] = acct["balance"] - req.amount
          logger.info('Transfer: Withdrawal successful, Current Balance ['+ str(acct["balance"]) +
                    '] for request ['+ req.get_request_id_str() + ']')
          ack = Reply("Transfer: Withdrawal Successful: ", acct["balance"],req.request_id)
        else:
          logger.error(' Insufficient Balance: Operation Unsuccessful - Current Balance ['+ str(acct["balance"]) +
        #            '] for request ['+ strr(req.request_id) +']')
       #   ack = Reply("Insufficient Balance, Transaction Transfer unsuccessful",acct["balance"], req.request_id)

                    '] for request ['+ req.get_request_id_str() +']')
          ack = Reply("Insufficient Balance, Transaction Transfer unsuccessful",acct["balance"], req.request_id)
        update_history(req, ack)
        return ack
      elif int(req.dest_bank) == self.bank_num:
        acct = self.account_details[req.account_no]
        acct["balance"] = acct["balance"] + req.amount
        logger.info('Deposit operation successful as part of transfer, Current Balance ['+ str(acct["balance"]) +
                    '] for request ['+ strr(req.request_id) +']')

        #ack = Reply("Transfer Operation successful", acct["balance"], req.request_id)
        ack = Reply("Transfer Operation successful",req.updated_balance, req.request_id)
        update_history(req, ack)
        return ack
      else:
        logger.info("Could not process transfer - bank numbers not matching..")


    def alive(threadname, delay):
      while True:
        time.sleep(delay)
        send(('Iamalive', threadname), to= master)



#################################################################################
    def initializelogger(logfile):
            logfilepath = '../logs/server/'
            logext = '.log'
            logger = logging.getLogger(logfile)
            logfilepath = '../logs/server/' + logfile + logext
            os.path.isfile(logfilepath)
            if (os.path.exists(logfilepath)):
                os.remove(logfilepath)
            hdlr = logging.FileHandler(logfilepath)
            formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
            hdlr.setFormatter(formatter)
            logger.addHandler(hdlr)
            logger.setLevel(logging.INFO)

# Server main
    def run():
      initializelogger("server.%d.%d" % (self.bank_num, self.server_num))

      if self.typeofserver == "extend":
        logger.info("Process for extending "+str(self.id)+" initiated")
        send(('ExtendChain', self.bank_num, self.id), to=master)

      logger.info('Server (%s) initiated' %(self.id))
      _thread.start_new_thread(alive, (self.id, 1, ))
      await(self.messages == self.count)
      logger.warn('Server crashing..')

      #await (some(received(('SERVER_NEVER_TERMINATE',_))))

